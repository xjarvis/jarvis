package com.ximba.jarvis;

// Java
import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.*;

// TestNG
import org.testng.annotations.*;

// Jarvis
import com.ximba.common.*;
import com.ximba.jarvis.Message;
import com.ximba.jarvis.ToText;

/**
 * Tests for ToText class.
 */
@Test(groups="totext")
public class TestToText
{
    private final Logger log = Logger.getLogger("TestToText");
    Setup setup = new Setup();

    Class   cls       = null;
    Field   field     = null;
    ToText  totext    = null;
    String  dataPath  = null;
    String  audioFile = "crucial_time.flac";

    /*
     * ------------------------------------------------------------
     * Before/After methods
     * ------------------------------------------------------------
     */
    @BeforeClass
    private void beforeClass() throws Exception
    {
        setup.init();
        setup.setupLog();
    }

    @AfterClass
    private void afterClass() throws Exception
    {
        setup.shutdownLog();
    }

    @BeforeMethod
    @SuppressWarnings("unchecked")
    private void beforeMethod() throws Exception
    {
        // Create new instance and clear configuration object
        totext = new ToText();
        cls = totext.getClass();
        dataPath = System.getenv("DATAPATH");
    }

    @AfterMethod
    private void afterMethod() throws Exception
    {
        // Teardown after each test.
        setup.cleanup();
    }

    /*
     * ------------------------------------------------------------
     * Tests
     * ------------------------------------------------------------
     */

    // setup
    @Test(description="setup")
    public void totext1()
    {
        log.info("--- totext1 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("totext1") )
            return;

        assert totext.setup(): "Failed setup.";
    }

    // start
    @Test(description="start / shutdown")
    public void totext2()
    {
        log.info("--- totext2 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("totext2") )
            return;

        try {
            assert totext.start(): "Failed start.";
            field = cls.getDeclaredField("runComplete");
            field.setAccessible(true);
            assert !field.getBoolean(totext): "runComplete is not false";

            totext.shutdown();
            Thread.sleep(100);
            assert field.getBoolean(totext): "runComplete is not true";
        }
        catch(Exception e) {
            assert false: "Exception: " + e.getMessage();
        }
    }

    // submit
    @Test(description="submit")
    @SuppressWarnings("unchecked")
    public void totext3()
    {
        log.info("--- totext3 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("totext3") )
            return;

        Message msg = new Message();
        totext.submit( msg );
        try {
            field = cls.getDeclaredField("msgs");
            field.setAccessible(true);
            ConcurrentLinkedQueue<Message> msgs = (ConcurrentLinkedQueue<Message>)field.get(totext);
            assert ( msgs.size() == 1 ): "Submission failed.";
        }
        catch(Exception e) {
            assert false: "Exception: " + e.getMessage();
        }
    }

    // process
    @Test(description="process")
    @SuppressWarnings("unchecked")
    public void totext4()
    {
        log.info("--- totext4 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("totext4") )
            return;

        // Set the Google API version to use.
        value = System.getenv("GAPIV");
        if ( value == null )
            value = "1";
        String[] args = {
            "-gapiv",
            value,
        };
        Cli cli = new Cli();
        cli.parseCmdLine( args );
        log.info("Using Google API Version: " + Cli.get(Cli.S_GAPIV));

        // Copy the data file to the audio dir.
        String filePath = Cli.get(Cli.S_AUDIODIR) + File.separator + audioFile;
        File destFile = new File(filePath);
        try { FileUtils.copyFile( new File(dataPath+File.separator+audioFile), destFile ); }
        catch(Exception e) { assert false: "Failed to copy audio file from data dir to audio dir."; }
        assert destFile.exists(): "Failed to copy audio file to audio dir.";

        // Create the message to process
        Message msg = new Message();
        msg.addData(Message.S_FILE, audioFile);

        // call the process method manually.
        setup.setPrefix("any");
        try {
            Method method = cls.getDeclaredMethod("process", new Class[] { Message.class });
            method.setAccessible(true);
            method.invoke(totext, new Object[] { msg });
        }
        catch(Exception e) {
            assert false: "process() caused exception: " + e.getMessage();
        }
        String logs = setup.getLog();
        assert !logs.contains("No such file"): "No such file error.";
        assert logs.contains("Response:"): "No response from Google.";
    }
}
