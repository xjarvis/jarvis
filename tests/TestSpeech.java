package com.ximba.jarvis;

// Java
import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import javax.sound.sampled.*;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.*;

// TestNG
import org.testng.annotations.*;

// Jarvis
import com.ximba.common.*;
import com.ximba.jarvis.Speech;

/**
 * Tests for Speech class.
 */
@Test(groups="speech")
public class TestSpeech
{
    private final Logger log = Logger.getLogger("TestSpeech");
    Setup setup = new Setup();

    Class   cls     = null;
    Field   field   = null;
    Speech  speech  = null;

    /*
     * ------------------------------------------------------------
     * Before/After methods
     * ------------------------------------------------------------
     */
    @BeforeClass
    private void beforeClass() throws Exception
    {
        setup.init();
        setup.setupLog();
    }

    @AfterClass
    private void afterClass() throws Exception
    {
        setup.shutdownLog();
    }

    @BeforeMethod
    @SuppressWarnings("unchecked")
    private void beforeMethod() throws Exception
    {
        // Create new instance and clear configuration object
        speech = new Speech();
        cls = speech.getClass();
    }

    @AfterMethod
    private void afterMethod() throws Exception
    {
        // Teardown after each test.
        setup.cleanup();
    }

    /*
     * ------------------------------------------------------------
     * Tests
     * ------------------------------------------------------------
     */

    // setup
    @Test(description="setup")
    public void speech1()
    {
        log.info("--- speech1 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("speech1") )
            return;

        assert speech.setup(): "Failed setup.";

        try {
            field = cls.getDeclaredField("audioFormat");
            field.setAccessible(true);
            assert (field.get(speech) != null): "audioFormat was not set";

            field = cls.getDeclaredField("line");
            field.setAccessible(true);
            assert (field.get(speech) != null): "line was not set";
        }
        catch(Exception e) {
            assert false: "Exception: " + e.getMessage();
        }
    }

    // start
    @Test(description="start / shutdown")
    public void speech2()
    {
        log.info("--- speech2 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("speech2") )
            return;

        assert speech.setup(): "Failed setup.";
        try {
            assert speech.start(): "Failed start.";
            field = cls.getDeclaredField("runComplete");
            field.setAccessible(true);
            assert !field.getBoolean(speech): "runComplete is not false";

            field = cls.getDeclaredField("line");
            field.setAccessible(true);
            TargetDataLine line = (TargetDataLine)field.get(speech);
            assert line.isRunning(): "line is not running";

            field = cls.getDeclaredField("writeIsRunning");
            field.setAccessible(true);
            boolean writeIsRunning = (boolean)field.getBoolean(speech);
            assert writeIsRunning: "writeIsRunning is not true";

            speech.shutdown();
            Thread.sleep(100);

            field = cls.getDeclaredField("runComplete");
            field.setAccessible(true);
            assert field.getBoolean(speech): "runComplete is not true";

            field = cls.getDeclaredField("writeIsRunning");
            field.setAccessible(true);
            writeIsRunning = (boolean)field.getBoolean(speech);
            assert !writeIsRunning: "writeIsRunning is not false";

            field = cls.getDeclaredField("line");
            field.setAccessible(true);
            line = (TargetDataLine)field.get(speech);
            assert !line.isRunning(): "line is not stopped";

        }
        catch(Exception e) {
            assert false: "Exception: " + e.getMessage();
        }
    }

    // speech3 - verify audio recording was written to file.
    @Test(description="Verify Audio File Writes")
    public void speech3()
    {
        log.info("--- speech3 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("speech3") )
            return;

        // Initialize configuration directories
        String[] args = null;
        Cli cli = new Cli();
        cli.parseCmdLine( args );
        try { FileUtils.cleanDirectory( new File(Cli.get(Cli.S_AUDIODIR)) ); }
        catch(Exception e) { assert false: "Failed to cleanup audio dir before test."; }

        // Capture logs 
        setup.setPrefix("any");

        // Get currently defined minimum level 
        float minLevel = 0F;
        try {
            field = cls.getDeclaredField("minLevel");
            field.setAccessible(true);
            minLevel = (float)field.get(speech);
        }
        catch(Exception e) {
            assert false: "Can't get default level";
        }

        assert speech.setup(): "Failed setup.";
        try {
            assert speech.start(): "Failed start.";

            log.info("");
            log.info("==================================================");
            log.info("Say something loudly.");
            log.info("==================================================");
            log.info("");
            Thread.sleep(7000);

            // Check logs for valid recording
            boolean levelOK = false;
            boolean writeOK = false;
            String[] logs = setup.getLog().split("\n");
            if ( (logs!= null) && (logs.length>0) )
            {
                for(int i=0; i<logs.length; i++)
                {
                    if (logs[i].contains("Audio level for inbound buffer") )
                    {
                        String[] fields= logs[i].split(":");
                        float level = Float.parseFloat( fields[1] );
                        if ( level > minLevel )
                            levelOK = true;
                    }
                    if (logs[i].contains("Writing snippet of") )
                        writeOK = true;
                }
            }
            assert levelOK: "You didn't speak loud enough.";
            assert writeOK: "Failed to write audio file.";

            speech.shutdown();
            Thread.sleep(100);
        }
        catch(Exception e) {
            assert false: "Exception: " + e.getMessage();
        }
    }
}
