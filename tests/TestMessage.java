package com.ximba.jarvis;

// Java
import java.lang.reflect.*;
import java.util.*;
import org.apache.log4j.*;

// TestNG
import org.testng.annotations.*;

// Jarvis
import com.ximba.common.*;
import com.ximba.jarvis.Message;

/**
 * Tests for Message class.
 */
@Test(groups="message")
public class TestMessage
{
    private final Logger log = Logger.getLogger("TestMessage");
    Setup setup = new Setup();

    Class   cls       = null;
    Field   field     = null;
    Message msg       = null;
    String  dataKey   = "MSGKEY";
    String  dataValue = "blah";
    byte[]  byteValue = {0x01, 0x02, 0x03};

    /*
     * ------------------------------------------------------------
     * Before/After methods
     * ------------------------------------------------------------
     */
    @BeforeClass
    private void beforeClass() throws Exception
    {
        setup.init();
        setup.setupLog();
    }

    @AfterClass
    private void afterClass() throws Exception
    {
        setup.shutdownLog();
    }

    @BeforeMethod
    @SuppressWarnings("unchecked")
    private void beforeMethod() throws Exception
    {
        // Create new instance and clear configuration object
        msg = new Message();
        cls = msg.getClass();
    }

    @AfterMethod
    private void afterMethod() throws Exception
    {
        // Teardown after each test.
        setup.cleanup();
    }

    /*
     * ------------------------------------------------------------
     * Tests
     * ------------------------------------------------------------
     */

    // string addData
    @Test(description="String addData/getData/toString")
    public void message1()
    {
        log.info("--- message1 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("message1") )
            return;

        try { msg.addData ( dataKey, dataValue ); }
        catch (NullPointerException npe) {
            assert false: "Adding data failed due to NullPointerException.";
        }
        Map<String,String> data = msg.getData();
        assert ( data.size() == 1 ): "Added data is missing.";

        String keyValues = msg.toString();
        keyValues = keyValues.replace("\n","");
        assert ( keyValues != null ): "No key/values found in data";
        String[] fields = keyValues.split(":");
        assert ( fields[0].equals(dataKey) ):"Key does not match.";
        assert ( fields[1].equals(dataValue) ):"Value does not match.";
    }

    // byte addData
    @Test(description="byte addData/getData")
    public void message2()
    {
        log.info("--- message2 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("message2") )
            return;

        try { msg.addData ( dataKey, byteValue ); }
        catch (NullPointerException npe) {
            assert false: "Adding data failed due to NullPointerException.";
        }
        Map<String,byte[]> data = msg.getByteData();
        assert ( data.size() == 1 ): "Added data is missing.";
        byte[] byteData = data.get(dataKey);
        assert ( byteData.length == 3 ): "byte data length invalid: wanted 3, got " + byteData.length;
    }

    // string removeData
    @Test(description="string removeData")
    public void message3()
    {
        log.info("--- message3 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("message3") )
            return;

        try { msg.addData ( dataKey, dataValue ); }
        catch (NullPointerException npe) {
            assert false: "Adding data failed due to NullPointerException.";
        }
        Map<String,String> data = msg.getData();
        assert ( data.size() == 1 ): "Added data is missing.";
        msg.removeData ( dataKey ) ;
        data = msg.getData();
        assert ( data.size() == 0 ): "Removal of data item failed.";
    }

    // byte removeData
    @Test(description="byte removeData")
    public void message4()
    {
        log.info("--- message4 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("message4") )
            return;

        try { msg.addData ( dataKey, byteValue ); }
        catch (NullPointerException npe) {
            assert false: "Adding data failed due to NullPointerException.";
        }
        Map<String,byte[]> data = msg.getByteData();
        assert ( data.size() == 1 ): "Added data is missing.";
        msg.removeByteData ( dataKey ) ;
        data = msg.getByteData();
        assert ( data.size() == 0 ): "Removal of data item failed.";
    }

    // string getDataItem
    @Test(description="string getDataItem")
    public void message5()
    {
        log.info("--- message5 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("message5") )
            return;

        try { msg.addData ( dataKey, dataValue ); }
        catch (NullPointerException npe) {
            assert false: "Adding data failed due to NullPointerException.";
        }
        Map<String,String> data = msg.getData();
        assert ( data.size() == 1 ): "Added data is missing.";
        value = msg.getDataItem ( dataKey ) ;
        assert (value != null ): "Failed to find valid key.";
        assert value.equals(dataValue): "Value mismatch.";
    }

    // byte getDataItem
    @Test(description="byte getDataItem")
    public void message6()
    {
        log.info("--- message6 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("message6") )
            return;

        try { msg.addData ( dataKey, byteValue ); }
        catch (NullPointerException npe) {
            assert false: "Adding data failed due to NullPointerException.";
        }
        Map<String,byte[]> data = msg.getByteData();
        assert ( data.size() == 1 ): "Added data is missing.";
        byte[] bytes = msg.getByteDataItem ( dataKey ) ;
        assert (bytes != null ): "Failed to find valid key.";
        for(int i=0; i<byteValue.length; i++)
            assert ( bytes[i] == byteValue[i] ) : "Byte mismatch @ index " + i;
    }

    // Clone constructor
    @Test(description="Clone constructor")
    public void message7()
    {
        log.info("--- message7 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("message7") )
            return;

        try { msg.addData ( dataKey, dataValue ); }
        catch (NullPointerException npe) {
            assert false: "Adding data failed due to NullPointerException.";
        }
        try { msg.addData ( dataKey, byteValue ); }
        catch (NullPointerException npe) {
            assert false: "Adding data failed due to NullPointerException.";
        }

        Message cloneMsg = new Message( msg );

        String str1 = msg.toString();
        String str2 = cloneMsg.toString();
        assert ( str1 != null ): "Source properties are null.";
        assert ( str2 != null ): "Clone properties are null.";
        assert ( str1.equals(str2) ): "String properties don't match.";

        byte[] bytes1 = msg.getByteDataItem(dataKey);
        byte[] bytes2 = cloneMsg.getByteDataItem(dataKey);
        assert (bytes1 != null ): "Failed to find valid key for msg.";
        assert (bytes2 != null ): "Failed to find valid key for clone.";
        for(int i=0; i<bytes1.length; i++)
            assert ( bytes1[i] == bytes2[i] ) : "Byte mismatch @ index " + i;
    }

}
