package com.ximba.jarvis;

// Java
import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.*;

// TestNG
import org.testng.annotations.*;

// Jarvis
import com.ximba.common.*;
import com.ximba.jarvis.Message;
import com.ximba.jarvis.Speaker;

/**
 * Tests for Speaker class.
 */
@Test(groups="speaker")
public class TestSpeaker
{
    private final Logger log = Logger.getLogger("TestSpeaker");
    Setup setup = new Setup();

    Class   cls       = null;
    Field   field     = null;
    Speaker  speaker  = null;

    /*
     * ------------------------------------------------------------
     * Before/After methods
     * ------------------------------------------------------------
     */
    @BeforeClass
    private void beforeClass() throws Exception
    {
        setup.init();
        setup.setupLog();
    }

    @AfterClass
    private void afterClass() throws Exception
    {
        setup.shutdownLog();
    }

    @BeforeMethod
    @SuppressWarnings("unchecked")
    private void beforeMethod() throws Exception
    {
        // Create new instance and clear configuration object
        speaker = new Speaker();
        cls = speaker.getClass();
    }

    @AfterMethod
    private void afterMethod() throws Exception
    {
        // Teardown after each test.
        setup.cleanup();
    }

    /*
     * ------------------------------------------------------------
     * Tests
     * ------------------------------------------------------------
     */

    // setup
    @Test(description="setup")
    public void speaker1()
    {
        log.info("--- speaker1 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("speaker1") )
            return;

        assert speaker.setup(): "Failed setup.";
    }

    // start
    @Test(description="start / shutdown")
    public void speaker2()
    {
        log.info("--- speaker2 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("speaker2") )
            return;

        try {
            assert speaker.start(): "Failed start.";
            field = cls.getDeclaredField("runComplete");
            field.setAccessible(true);
            assert !field.getBoolean(speaker): "runComplete is not false";

            speaker.shutdown();
            Thread.sleep(100);
            assert field.getBoolean(speaker): "runComplete is not true";
        }
        catch(Exception e) {
            assert false: "Exception: " + e.getMessage();
        }
    }

    // submit
    @Test(description="submit")
    @SuppressWarnings("unchecked")
    public void speaker3()
    {
        log.info("--- speaker3 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("speaker3") )
            return;

        Message msg = new Message();
        speaker.submit( msg );
        try {
            field = cls.getDeclaredField("msgs");
            field.setAccessible(true);
            ConcurrentLinkedQueue<Message> msgs = (ConcurrentLinkedQueue<Message>)field.get(speaker);
            assert ( msgs.size() == 1 ): "Submission failed.";
        }
        catch(Exception e) {
            assert false: "Exception: " + e.getMessage();
        }
    }

    // process
    @Test(description="process")
    @SuppressWarnings("unchecked")
    public void speaker4()
    {
        log.info("--- speaker4 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("speaker4") )
            return;

        String[] args = null;
        Cli cli = new Cli();
        cli.parseCmdLine( args );

        // Create the message to process
        Message msg = new Message();
        msg.addData(Message.S_SPEAK, "Good morning, sir");

        // call the process method manually.
        setup.setPrefix("any");
        try {
            Method method = cls.getDeclaredMethod("process", new Class[] { Message.class });
            method.setAccessible(true);
            method.invoke(speaker, new Object[] { msg });
        }
        catch(Exception e) {
            assert false: "process() caused exception: " + e.getMessage();
        }
        String logs = setup.getLog();
    }
}
