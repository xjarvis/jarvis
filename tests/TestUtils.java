package com.ximba.jarvis;

// TestNG
import org.testng.annotations.*;

// Java
import java.io.File;
import org.apache.log4j.*;

// Jarvis
import com.ximba.common.Utils;

/**
 * Tests for Utils class.
 */
@Test(groups="utils")
public class TestUtils
{
    private final Logger log = Logger.getLogger("TestUtils");
    Setup setup = new Setup();

    File dir             = new File("/tmp");
    File Bdir            = new File("/badtmp");
    String[] env         = { "SHELL=/bin/sh" };
    String[] Benv        = { "SHELL /bad" };
    String[] cmd         = { "ls -l /dev/zero" };
    String[] Bcmd        = { "zzz -l /dev/zero" };
    String[] cmdSplit    = { "ls", "-l", "/dev/zero" };
    String[] BcmdSplit   = { "zzz", "-l", "/dev/zero" };

    String   ipaddr      = "192.168.1.1";
    String[] Bipaddr     = { "192.168.1", "192-168.1.1", "19a.168.1.1", "192.1a8.1.1", "192.168.a.1", "192.168.1.a" };

    /*
     * ------------------------------------------------------------
     * Before/After methods
     * ------------------------------------------------------------
     */
    @BeforeClass
    private void beforeClass() throws Exception
    {
        setup.init();
        setup.setupLog();
    }

    @AfterClass
    private void afterClass() throws Exception
    {
        setup.shutdownLog();
    }

    @AfterMethod
    private void afterMethod() throws Exception
    {
        // Teardown after each test.
        setup.cleanup();
    }


    /*
     * ------------------------------------------------------------
     * isIPValid() tests
     * ------------------------------------------------------------
     */

    @Test(groups="utils", description="Test valid IP string")
    public void validIP()
    {
        log.info("--- u1 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u1") )
            return;

        Utils utils = new Utils();
        try {
            if ( !utils.isIPValid( ipaddr ) )
                assert ( 1 == 0 ): ipaddr + " is an invalid IP address but should be valid";
        }
        catch (Exception e) {
            assert ( 1 == 0 ):ipaddr + " is an invalid IP address but should be valid";
        }
    }

    @Test(groups="utils", description="Test invalid IP strings")
    public void invalidIP()
    {
        log.info("--- u2 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u2") )
            return;

        Utils utils = new Utils();
        for(int i=0; i<Bipaddr.length; i++)
        {
            try {
                if ( !utils.isIPValid( Bipaddr[i] ) )
                    assert ( 1 == 0 ): Bipaddr[i] + " is an invalid IP address but did not throw an exception";
                else
                    assert ( 1 == 0 ): Bipaddr[i] + " should have thrown an exception";
            }
            catch (Exception e) { }
        }
    }

    /*
     * ------------------------------------------------------------
     * runCmd() positive tests
     * ------------------------------------------------------------
     */

    @Test(groups="runCmd", description="Run command, single string")
    public void runCmd1()
    {
        log.info("--- u3 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u3") )
            return;

        try {
            String[] results = Utils.runCmd( cmd[0] );
            assert ( results[0] != null );
            assert ( results[1] != null );
            assert ( results[0].length() > 0  );
            assert ( results[1].length() == 0  );
        }
        catch (Exception e) {
            assert ( 1 == 0 );
        }
    }

    @Test(groups="runCmd", description="Run command, single string, null env, dir")
    public void runCmd2()
    {
        log.info("--- u4 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u4") )
            return;

        try {
            String[] results = Utils.runCmd( cmd[0], null, dir );
            assert ( results[0] != null );
            assert ( results[1] != null );
            assert ( results[0].length() > 0  );
            assert ( results[1].length() == 0  );
        }
        catch (Exception e) {
            assert ( 1 == 0 );
        }
    }

    @Test(groups="runCmd", description="Run command, single string, env, null dir")
    public void runCmd3()
    {
        log.info("--- u5 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u5") )
            return;

        try {
            String[] results = Utils.runCmd( cmd[0], env, null );
            assert ( results[0] != null );
            assert ( results[1] != null );
            assert ( results[0].length() > 0  );
            assert ( results[1].length() == 0  );
        }
        catch (Exception e) {
            assert ( 1 == 0 );
        }
    }

    @Test(groups="runCmd", description="Run command, single string, env, dir")
    public void runCmd4()
    {
        log.info("--- u6 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u6") )
            return;

        try {
            String[] results = Utils.runCmd( cmd[0], env, dir );
            assert ( results[0] != null );
            assert ( results[1] != null );
            assert ( results[0].length() > 0  );
            assert ( results[1].length() == 0  );
        }
        catch (Exception e) {
            assert ( 1 == 0 );
        }
    }

    @Test(groups="runCmd", description="Run command, split string")
    public void runCmd5()
    {
        log.info("--- u7 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u7") )
            return;

        try {
            String[] results = Utils.runCmd( cmdSplit );
            assert ( results[0] != null );
            assert ( results[1] != null );
            assert ( results[0].length() > 0  );
            assert ( results[1].length() == 0  );
        }
        catch (Exception e) {
            assert ( 1 == 0 );
        }
    }

    @Test(groups="runCmd", description="Run command, split string, null env, dir")
    public void runCmd6()
    {
        log.info("--- u8 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u8") )
            return;

        try {
            String[] results = Utils.runCmd( cmdSplit, null, dir );
            assert ( results[0] != null );
            assert ( results[1] != null );
            assert ( results[0].length() > 0  );
            assert ( results[1].length() == 0  );
        }
        catch (Exception e) {
            assert ( 1 == 0 );
        }
    }

    @Test(groups="runCmd", description="Run command, split string, env, null dir")
    public void runCmd7()
    {
        log.info("--- u9 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u9") )
            return;

        try {
            String[] results = Utils.runCmd( cmdSplit, env, null );
            assert ( results[0] != null );
            assert ( results[1] != null );
            assert ( results[0].length() > 0  );
            assert ( results[1].length() == 0  );
        }
        catch (Exception e) {
            assert ( 1 == 0 );
        }
    }

    @Test(groups="runCmd", description="Run command, split string, env, dir")
    public void runCmd8()
    {
        log.info("--- u10 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u10") )
            return;

        try {
            String[] results = Utils.runCmd( cmdSplit, env, dir );
            assert ( results[0] != null );
            assert ( results[1] != null );
            assert ( results[0].length() > 0  );
            assert ( results[1].length() == 0  );
        }
        catch (Exception e) {
            assert ( 1 == 0 );
        }
    }

    /*
     * ------------------------------------------------------------
     * runCmd() negative tests: bad command
     * ------------------------------------------------------------
     */

    @Test(groups="runCmd", description="Bad command, single string")
    public void runCmd1n()
    {
        log.info("--- u11 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u11") )
            return;

        try {
            String[] results = Utils.runCmd( Bcmd[0] );
            assert true: Bcmd[0] + " should have generated exception, but didn't.";
        }
        catch (Exception e) {
            if ( !e.getMessage().contains("Failed to run command") )
                assert ( 1 == 0 ): "Bad command generated exception, but wrong one: " + e.getMessage();
        }
    }

    @Test(groups="runCmd", description="Bad command, single string, null env, dir")
    public void runCmd2n()
    {
        log.info("--- u12 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u12") )
            return;

        try {
            String[] results = Utils.runCmd( Bcmd[0], null, dir );
            assert true: Bcmd[0] + " should have generated exception, but didn't.";
        }
        catch (Exception e) {
            if ( !e.getMessage().contains("Failed to run command") )
                assert ( 1 == 0 ): "Bad command generated exception, but wrong one: " + e.getMessage();
        }
    }

    @Test(groups="runCmd", description="Bad command, single string, env, null dir")
    public void runCmd3n()
    {
        log.info("--- u13 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u13") )
            return;

        try {
            String[] results = Utils.runCmd( Bcmd[0], env, null );
            assert true: Bcmd[0] + " should have generated exception, but didn't.";
        }
        catch (Exception e) {
            if ( !e.getMessage().contains("Failed to run command") )
                assert ( 1 == 0 ): "Bad command generated exception, but wrong one: " + e.getMessage();
        }
    }

    @Test(groups="runCmd", description="Bad command, single string, env, dir")
    public void runCmd4n()
    {
        log.info("--- u14 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u14") )
            return;

        try {
            String[] results = Utils.runCmd( Bcmd[0], env, dir );
            assert true: Bcmd[0] + " should have generated exception, but didn't.";
        }
        catch (Exception e) {
            if ( !e.getMessage().contains("Failed to run command") )
                assert ( 1 == 0 ): "Bad command generated exception, but wrong one: " + e.getMessage();
        }
    }

    @Test(groups="runCmd", description="Bad command, split string")
    public void runCmd5n()
    {
        log.info("--- u15 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u15") )
            return;

        try {
            String[] results = Utils.runCmd( BcmdSplit );
            assert true: Bcmd[0] + " should have generated exception, but didn't.";
        }
        catch (Exception e) {
            if ( !e.getMessage().contains("Failed to run command") )
                assert ( 1 == 0 ): "Bad command generated exception, but wrong one: " + e.getMessage();
        }
    }

    @Test(groups="runCmd", description="Bad command, split string, null env, dir")
    public void runCmd6n()
    {
        log.info("--- u16 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u16") )
            return;

        try {
            String[] results = Utils.runCmd( BcmdSplit, null, dir );
            assert true: Bcmd[0] + " should have generated exception, but didn't.";
        }
        catch (Exception e) {
            if ( !e.getMessage().contains("Failed to run command") )
                assert ( 1 == 0 ): "Bad command generated exception, but wrong one: " + e.getMessage();
        }
    }

    @Test(groups="runCmd", description="Bad command, split string, env, null dir")
    public void runCmd7n()
    {
        log.info("--- u17 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u17") )
            return;

        try {
            String[] results = Utils.runCmd( BcmdSplit, env, null );
            assert true: Bcmd[0] + " should have generated exception, but didn't.";
        }
        catch (Exception e) {
            if ( !e.getMessage().contains("Failed to run command") )
                assert ( 1 == 0 ): "Bad command generated exception, but wrong one: " + e.getMessage();
        }
    }

    @Test(groups="runCmd", description="Bad command, split string, env, dir")
    public void runCmd8n()
    {
        log.info("--- u18 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u18") )
            return;

        try {
            String[] results = Utils.runCmd( BcmdSplit, env, dir );
            assert true: Bcmd[0] + " should have generated exception, but didn't.";
        }
        catch (Exception e) {
            if ( !e.getMessage().contains("Failed to run command") )
                assert ( 1 == 0 ): "Bad command generated exception, but wrong one: " + e.getMessage();
        }
    }

    /*
     * ------------------------------------------------------------
     * runCmd() negative tests: bad env
     * ------------------------------------------------------------
     */

    @Test(groups="runCmd", description="Run command, single string, bad env, dir")
    public void runCmd2e()
    {
        log.info("--- u19 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u19") )
            return;

        try {
            String[] results = Utils.runCmd( cmd[0], Benv, dir );
            assert true: Benv[0] + " should have generated exception, but didn't.";
        }
        catch (Exception e) {
            if ( !e.getMessage().contains("Failed to run command") )
                assert ( 1 == 0 ): "Bad ENV generated exception, but wrong one: " + e.getMessage();
        }
    }

    @Test(groups="runCmd", description="Run command, split string, bad env, dir")
    public void runCmd6e()
    {
        log.info("--- u20 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u20") )
            return;

        try {
            String[] results = Utils.runCmd( cmdSplit, Benv, dir );
            assert true: Benv[0] + " should have generated exception, but didn't.";
        }
        catch (Exception e) {
            if ( !e.getMessage().contains("Failed to run command") )
                assert ( 1 == 0 ): "Bad ENV generated exception, but wrong one: " + e.getMessage();
        }
    }

    /*
     * ------------------------------------------------------------
     * runCmd() negative tests: bad dir
     * ------------------------------------------------------------
     */

    @Test(groups="runCmd", description="Run command, single string, null env, bad dir")
    public void runCmd2d()
    {
        log.info("--- u21 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u21") )
            return;

        try {
            String[] results = Utils.runCmd( cmd[0], null, Bdir );
            assert true: Bdir + " should have generated exception, but didn't.";
        }
        catch (Exception e) {
            if ( !e.getMessage().contains("Failed to run command") )
                assert ( 1 == 0 ): "Bad dir generated exception, but wrong one: " + e.getMessage();
        }
    }

    @Test(groups="runCmd", description="Run command, split string, null env, bad dir")
    public void runCmd6d()
    {
        log.info("--- u22 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("u22") )
            return;

        try {
            String[] results = Utils.runCmd( cmdSplit, null, Bdir );
            assert true: Bdir + " should have generated exception, but didn't.";
        }
        catch (Exception e) {
            if ( !e.getMessage().contains("Failed to run command") )
                assert ( 1 == 0 ): "Bad dir generated exception, but wrong one: " + e.getMessage();
        }
    }

}
