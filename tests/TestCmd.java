package com.ximba.jarvis;

// Java
import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.log4j.*;

// TestNG
import org.testng.annotations.*;

// Jarvis
import com.ximba.common.*;
import com.ximba.jarvis.Cmd;

/**
 * Tests for Cmd class.
 */
@Test(groups="cmd")
public class TestCmd
{
    private final Logger log = Logger.getLogger("TestCmd");
    Setup setup = new Setup();

    Class   cls     = null;
    Field   field   = null;
    Cmd     cmd     = null;

    // V1 sample return data from a real Google Speech API session.
    String jsonV1 = "{\"status\":0,\"id\":\"\",\"hypotheses\":[{\"utterance\":\"this is the most crucial in series time of all history\",\"confidence\":0.90499514},{\"utterance\":\"this is the most crucial in Ceres time of all history\"},{\"utterance\":\"this is the most crucial in Ceres Time of all history\"},{\"utterance\":\"this is the most crucial in series. Time of all history\"},{\"utterance\":\"this is the most crucial in serious time of all history\"},{\"utterance\":\"this is the most crucial and furious time of all history\"},{\"utterance\":\"this is the most gruesome and furious time of all history\"}]}";

    // V2 sample return data from a real Google Speech API session.
    String jsonV2 = "{\"result\":[{\"alternative\":[{\"transcript\":\"this is the most crucial in series time of all history\",\"confidence\":0.90499514},{\"transcript\":\"this is the most crucial in Ceres time of all history\"},{\"transcript\":\"this is the most crucial in Ceres Time of all history\"},{\"transcript\":\"this is the most crucial in series. Time of all history\"},{\"transcript\":\"this is the most crucial in serious time of all history\"}],\"final\":true}],\"result_index\":0}";

    // V2: bad "result"
    String V2b1Title = "Bad result";
    String jsonV2b1 = "{\"rsult\":[{\"alternative\":[{\"transcript\":\"this is the most crucial in series time of all history\",\"confidence\":0.90499514},{\"transcript\":\"this is the most crucial in Ceres time of all history\"},{\"transcript\":\"this is the most crucial in Ceres Time of all history\"},{\"transcript\":\"this is the most crucial in series. Time of all history\"},{\"transcript\":\"this is the most crucial in serious time of all history\"}],\"final\":true}],\"result_index\":0}";

    // V2: bad "alternative"
    String V2b2Title = "Bad alternative";
    String jsonV2b2 = "{\"result\":[{\"lternative\":[{\"transcript\":\"this is the most crucial in series time of all history\",\"confidence\":0.90499514},{\"transcript\":\"this is the most crucial in Ceres time of all history\"},{\"transcript\":\"this is the most crucial in Ceres Time of all history\"},{\"transcript\":\"this is the most crucial in series. Time of all history\"},{\"transcript\":\"this is the most crucial in serious time of all history\"}],\"final\":true}],\"result_index\":0}";

    // V2: bad "transcript"
    String V2b3Title = "Bad transcript";
    String jsonV2b3 = "{\"result\":[{\"alternative\":[{\"trnscript\":\"this is the most crucial in series time of all history\",\"confidence\":0.90499514},{\"transcript\":\"this is the most crucial in Ceres time of all history\"},{\"transcript\":\"this is the most crucial in Ceres Time of all history\"},{\"transcript\":\"this is the most crucial in series. Time of all history\"},{\"transcript\":\"this is the most crucial in serious time of all history\"}],\"final\":true}],\"result_index\":0}";

    // V2: bad "confidence"
    String V2b4Title = "Bad confidence";
    String jsonV2b4 = "{\"result\":[{\"alternative\":[{\"transcript\":\"this is the most crucial in series time of all history\",\"cnfidence\":0.90499514},{\"transcript\":\"this is the most crucial in Ceres time of all history\"},{\"transcript\":\"this is the most crucial in Ceres Time of all history\"},{\"transcript\":\"this is the most crucial in series. Time of all history\"},{\"transcript\":\"this is the most crucial in serious time of all history\"}],\"final\":true}],\"result_index\":0}";

    /*
     * ------------------------------------------------------------
     * Before/After methods
     * ------------------------------------------------------------
     */
    @BeforeClass
    private void beforeClass() throws Exception
    {
        setup.init();
        setup.setupLog();
    }

    @AfterClass
    private void afterClass() throws Exception
    {
        setup.shutdownLog();
    }

    @BeforeMethod
    @SuppressWarnings("unchecked")
    private void beforeMethod() throws Exception
    {
        // Create new instance and clear configuration object
        cmd = new Cmd();
        cls = cmd.getClass();
    }

    @AfterMethod
    private void afterMethod() throws Exception
    {
        // Teardown after each test.
        setup.cleanup();
    }

    /*
     * ------------------------------------------------------------
     * Utility methods
     * ------------------------------------------------------------
     */

    /*
     * ------------------------------------------------------------
     * Tests
     * ------------------------------------------------------------
     */

    // setup
    @Test(description="setup")
    public void cmd1()
    {
        log.info("--- cmd1 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("cmd1") )
            return;

        assert cmd.setup(): "Failed setup.";
    }

    // start
    @Test(description="start / shutdown")
    public void cmd2()
    {
        log.info("--- cmd2 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("cmd2") )
            return;

        try {
            assert cmd.start(): "Failed start.";
            field = cls.getDeclaredField("runComplete");
            field.setAccessible(true);
            assert !field.getBoolean(cmd): "runComplete is not false";

            cmd.shutdown();
            Thread.sleep(100);
            assert field.getBoolean(cmd): "runComplete is not true";
        }
        catch(Exception e) {
            assert false: "Exception: " + e.getMessage();
        }
    }

    // process: v1
    @Test(description="process: v1")
    @SuppressWarnings("unchecked")
    public void cmd3()
    {
        log.info("--- cmd3 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("cmd3") )
            return;

        // Set the Google API version to use.
        setup.setGAPI("1");

        // Set the message to parse.
        Message msg = new Message();
        msg.addData(Message.S_JSON, jsonV1);

        // call the process method manually.
        setup.setPrefix("any");
        try {
            Method method = cls.getDeclaredMethod("process", new Class[] { Message.class });
            method.setAccessible(true);
            method.invoke(cmd, new Object[] { msg });
        }
        catch(Exception e) {
            e.printStackTrace();
            assert false: "process() caused exception: " + e.getMessage();
        }
        String logs = setup.getLog();

        assert logs.contains("Saved transcript"): "V1 Transcript not saved.";
        assert logs.contains("Saved confidence"): "V1 Confidence not saved.";
    }

    // process: v2
    @Test(description="process: v2")
    @SuppressWarnings("unchecked")
    public void cmd4()
    {
        log.info("--- cmd4 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("cmd4") )
            return;

        // Set the Google API version to use.
        setup.setGAPI("2");

        // Set the message to parse.
        Message msg = new Message();
        msg.addData(Message.S_JSON, jsonV2);

        // call the process method manually.
        setup.setPrefix("any");
        try {
            Method method = cls.getDeclaredMethod("process", new Class[] { Message.class });
            method.setAccessible(true);
            method.invoke(cmd, new Object[] { msg });
        }
        catch(Exception e) {
            e.printStackTrace();
            assert false: "process() caused exception: " + e.getMessage();
        }
        String logs = setup.getLog();

        assert logs.contains("Saved transcript"): "V1 Transcript not saved.";
        assert logs.contains("Saved confidence"): "V1 Confidence not saved.";
    }

    // process: Bad v2
    @Test(description="process: bad v2")
    @SuppressWarnings("unchecked")
    public void cmd5()
    {
        log.info("--- cmd5 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("cmd5") )
            return;

        // Get access to the process() method.
        Method method = null;
        try {
            method = cls.getDeclaredMethod("process", new Class[] { Message.class });
            method.setAccessible(true);
        }
        catch(Exception e){
            assert false: "Failed to retrieve access to process().";
        }

        // Set the Google API version to use.
        setup.setGAPI("2");
        setup.setPrefix("any");

        // Create and parse a bad message.
        log.info("Testing jsonV2b1");
        Message msg = new Message();
        msg.addData(Message.S_JSON, jsonV2b1);
        try { method.invoke(cmd, new Object[] { msg }); }
        catch(Exception e) {
            e.printStackTrace();
            assert false: "process() caused exception: " + e.getMessage();
        }
        String logs = setup.getLog();
        assert logs.contains("Missing result"): "V2 didn't missing result.";
        setup.clearLog();

        // Create and parse a bad message.
        log.info("Testing jsonV2b2");
        msg = new Message();
        msg.addData(Message.S_JSON, jsonV2b2);
        try { method.invoke(cmd, new Object[] { msg }); }
        catch(Exception e) {
            e.printStackTrace();
            assert false: "process() caused exception: " + e.getMessage();
        }
        logs = setup.getLog();
        assert logs.contains("Missing alternative"): "V2 didn't catch missing alternative.";
        setup.clearLog();

        // Create and parse a bad message.
        log.info("Testing jsonV2b3");
        msg = new Message();
        msg.addData(Message.S_JSON, jsonV2b3);
        try { method.invoke(cmd, new Object[] { msg }); }
        catch(Exception e) {
            e.printStackTrace();
            assert false: "process() caused exception: " + e.getMessage();
        }
        logs = setup.getLog();
        assert !logs.contains("Saved transcript"): "V2 should not have saved transcript.";
        setup.clearLog();

        // Create and parse a bad message.
        log.info("Testing jsonV2b4");
        msg = new Message();
        msg.addData(Message.S_JSON, jsonV2b4);
        try { method.invoke(cmd, new Object[] { msg }); }
        catch(Exception e) {
            e.printStackTrace();
            assert false: "process() caused exception: " + e.getMessage();
        }
        logs = setup.getLog();
        assert !logs.contains("Saved transcript"): "V2 should not have saved transcript.";
        setup.clearLog();
    }

}
