%define rev 1
%define prjdir opt/jarvis

Name: jarvis
Summary: The IoT butler
Version: [--VERSION--]
Release: %{rev}
Source: %{name}-%{version}.tar.gz
Vendor: Michael J. Hammel
License: Proprietary
URL:  https://gitlab.com/xjarvis/jarvis
Packager: Michael J. Hammel <mjhammel@graphcs-muse.org>

# Group is based on the Fedora Groups hierarchy.  See
# /usr/share/doc/rpm-<version>/GROUPS for details.
Group: Applications/Engineering

AutoReqProv: no
Requires: jdk
BuildRoot: %{_tmppath}/%{name}-root

%description
<project description>

%prep
%setup
                                                                                
%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/%{prjdir}
mkdir -p $RPM_BUILD_ROOT/usr/bin

cp -r jarvis/* $RPM_BUILD_ROOT/%{prjdir}
ln -s /%{prjdir}/scripts/jarvis.sh $RPM_BUILD_ROOT/usr/bin/jarvis

%clean
rm -rf $RPM_BUILD_ROOT

%files
%attr(0755,root,root) /%{prjdir}
/usr/bin/jarvis
/%{prjdir}/scripts/jarvis.sh
