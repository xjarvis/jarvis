#!/usr/bin/perl -w
# Find out what version of RPM to use.
#-----------------------------------------------------
use Getopt::Long;
use Cwd;

sub usage {
$USAGE =<<EOF;
$0 -p <pkgname> -d <dir> -i <installdir> -v <version> -s <specfile> -t <tmpdir> -a <arch>
where
-p <pkgname>     The name to give to the RPM package 
-d <dir>         The path to the directory which holds the files to be packaged
-i <dir>         The name of the directory (without path) that will hold the files after installation.
-v <version>     The version information for the package.
-s <specfile>    The specfile to use to create the RPM package.
-t <installdir>  The installation directory to use.
-a <architecture> Defaults to "unknown"
-verbose         Enabled verbose output
All arguments (except verbose) are required.
EOF

    print "$USAGE";
	exit(0);
}

my $clientDir = "";
my $versionInfo = "";
my $pkgName = "";
my $specFile = "";
my $installDir = "";
my $tmpDir = "";
my $verbose = 0;
my $dousage = 0;
my $arch = "unknown";
$okay = GetOptions ( "d:s"          => \$clientDir,
                     "p:s"          => \$pkgName,
                     "t:s"          => \$tmpDir,
                     "i:s"          => \$installDir,
                     "v:s"          => \$versionInfo,
                     "s:s"          => \$specFile,
                     "a:s"          => \$arch,
                     "h|?"          => \$dousage,
                     "verbose"      => \$verbose);
if ( !$okay )    { usage(); }
if ( $dousage )  { usage(); }

# Error check the command args.
if ( "$clientDir" eq "" )
{
    print "You must provide a client directory to package using the -d option.\n";
    exit(1);
}
if ( "$installDir" eq "" )
{
    print "You must provide the installation directory name using the -i option.\n";
    exit(1);
}
if ( "$pkgName" eq "" )
{
    print "You must provide a package name using the -p option.\n";
    exit(1);
}
if ( "$versionInfo" eq "" )
{
    print "You must provide the version information using the -v option.\n";
    exit(1);
}
if ( "$specFile" eq "" )
{
    print "You must provide the spec file using the -s option.\n";
    exit(1);
}
if ( "$tmpDir" eq "" )
{
    print "You must provide the temporary directory using the -t option.\n";
    exit(1);
}

# Check for required Files and Directories.
if ( ! -f "$specFile" )
{
    print "$specFile does not exist.\n";
    exit(1);
}
if ( ! -d "$clientDir" )
{
    print "$clientDir does not exist.\n";
    exit(1);
}
if ( ! -d "$tmpDir" )
{
    print "$tmpDir does not exist.\n";
    exit(1);
}

#-----------------------------------------------------
# RPM version checks

open(FD, "rpm --version | ");
my @data = <FD>;
close(FD);
my @rpmvers = split(/ /, $data[0]);
my $RPMVERSION = pop @rpmvers;
@data = split(/\./, $RPMVERSION);
my $MAJOR = $data[0];
my $MINOR = $data[1];
if ( ($MAJOR < 4) || ($MAJOR > 4) )
{
   print "## RPM version ${RPMVERSION} not supported\n";
   exit(1);
}
my $RPMCMD;
if ( $MINOR > 2 ) { $RPMCMD="rpmbuild"; }
else               { $RPMCMD="rpm";      }

#-----------------------------------------------------
# Build the RPM package.

# Create a temporary RPM build directory.
my $home = $ENV{HOME};

# Create a temporary RPM build directory.
my @builddirs = (
    "rpm",
    "rpm/BUILD",
    "rpm/RPMS",
    "rpm/RPMS/i386",
    "rpm/RPMS/x86_64",
    "rpm/SOURCES",
    "rpm/SPECS",
    "rpm/SRPMS"
    );
foreach(@builddirs) { mkdir("$tmpDir/$_") || die "Can't make $tmpDir/$_"; }
my $rpmmacrosExists = 0;
if ( -f "$home/.rpmmacros" )
{
    rename("$home/.rpmmacros", "$home/.rpmmacros.$$");
    $rpmmacrosExists = 1;
}
open(FD, ">$home/.rpmmacros");
print FD "%_topdir  $tmpDir/rpm\n";
close(FD);

$verbose && print("mkdir $pkgName-$versionInfo \n");
mkdir("$pkgName-$versionInfo");
$verbose && print("mkdir $pkgName-$versionInfo/$installDir \n");
mkdir("$pkgName-$versionInfo/$installDir");
$verbose && print("cp -r $clientDir/* $pkgName-$versionInfo/$installDir/ \n");
system("cp -r $clientDir/* $pkgName-$versionInfo/$installDir/");

my @lines;
open(FD, "$specFile");
while (<FD>)
{
    my $line = $_;
    $line =~ s!\[--VERSION--\]!$versionInfo!sg;
    push @lines, $line;
}
close(FD);
open(FD, ">$pkgName-$versionInfo/$specFile");
print FD @lines;
close(FD);

$ENV{'TAR_OPTIONS'} = '--wildcards';

$verbose && print("tar cvzf $pkgName-$versionInfo.tar.gz $pkgName-$versionInfo \n");
$verbose && system("tar cvzf $pkgName-$versionInfo.tar.gz $pkgName-$versionInfo");
!$verbose && system("tar czf $pkgName-$versionInfo.tar.gz $pkgName-$versionInfo");

if ( !$verbose ) { 
	print("$RPMCMD --quiet -ta $pkgName-$versionInfo.tar.gz >/dev/null 2>&1 \n");
	system("$RPMCMD --quiet -ta $pkgName-$versionInfo.tar.gz ");
}
else {
	print("$RPMCMD -ta $pkgName-$versionInfo.tar.gz >/dev/null 2>&1 \n");
	system("$RPMCMD -ta $pkgName-$versionInfo.tar.gz ");
}

$verbose && print("rename $tmpDir/rpm/RPMS/$arch/$pkgName-$versionInfo-1.$arch.rpm $tmpDir/$pkgName-$versionInfo-1.$arch.rpm \n");
rename("$tmpDir/rpm/RPMS/$arch/$pkgName-$versionInfo-1.$arch.rpm", "$tmpDir/$pkgName-$versionInfo-1.$arch.rpm");

$verbose && print("rm -rf $pkgName-$versionInfo $tmpDir/rpm $pkgName-$versionInfo.tar.gz \n");
system("rm -rf $pkgName-$versionInfo $tmpDir/rpm $pkgName-$versionInfo.tar.gz");

if ( $rpmmacrosExists )
{
    $verbose && print("rename $home/.rpmmacros.$$ $home/.rpmmacros \n");
    rename("$home/.rpmmacros.$$", "$home/.rpmmacros");
}
else
{
    $verbose && print("unlink $home/.rpmmacros \n");
    unlink("$home/.rpmmacros");
}

