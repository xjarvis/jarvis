package com.ximba.common;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.*;
import java.net.*;
import org.apache.log4j.Logger;

/**
 * <p> 
 * General purpose utility functions.
 * </p>
 * @author Michael J. Hammel
 * @since 1.0
 */

public class Utils {

    private static final Logger log = Logger.getLogger("com.ximba.common.Utils");
    private String errorMsg = null;

    // Timestamps can be used for event processing.
    private static long initTimeStamp = 0;

    // Used when converting byte arrays to hex.
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    /*
     * ---------------------------------------------------------------
     * Utility
     * ---------------------------------------------------------------
     */

    /**
     * Read a file into a string.
     */
    static public String readFile(String path, Charset encoding) throws IOException 
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    /**
     * Set the starting timestamp for event timing.
     */
    static public void initTimeStamp() {
        initTimeStamp = Calendar.getInstance().getTimeInMillis();
    };

    /**
     * Reset the timestamp for event timing.  This sets it back to 0, effectively disabling it.
     */
    static public void resetTimeStamp() {
        initTimeStamp = 0;
    };

    /**
     * Get offset from initialized timestamp.  
     * @return The number of elapsed milliseconds or 0 if not initialized.
     */
    static public long getTimeStamp() {
        if ( initTimeStamp == 0 )
            return 0;
        else
            return ( Calendar.getInstance().getTimeInMillis() - initTimeStamp );
    };

    /**
     * Access the class variable used to store the current error message.
     * @return The error message as a String.
     */
    public String getErrorMsg() { return errorMsg; };

    /**
     * Check the validity of an IP Address.  An IP address is considered valid if it 
     * ha a dotted quad format (IPV4).
     * @param ipaddr  The string to validate 
     * @return True if the address is a valid IPV4 address.
     * @throws Exception if the address is not valid, including the reason in the exception message.
     * @note Only supports IPV4 formatted addresses.
     */
    public boolean isIPValid( String ipaddr ) throws Exception
    {
        String[] parts = ipaddr.split( "\\." );
        if ( parts.length != 4 )
        {
            errorMsg = "IP Address does not have 4 octets.";
            log.error(errorMsg);
            throw new Exception( errorMsg );
        }

        int idx = 1;
        for ( String s : parts )
        {
            if ( s.matches( "[0-9]{1,3}" ) == false )
            {
                errorMsg = "IP Address octet " + idx + " is not numeric.";
                log.error(errorMsg);
                throw new Exception( errorMsg );
            }

            int i = Integer.parseInt( s );
            if ( (i < 0) || (i > 255) )
            {
                errorMsg = "IP octets " + idx + " is not valid.";
                log.error(errorMsg);
                throw new Exception( errorMsg );
            }

            idx++;
        }
        return true;
    }

    /**
     * Run a command from the specified directory (and possibly with altered environment) and return the output.
     * @param cmd       The external command to run.
     * @param env       Environment variables in the form NAME=VALUE
     * @param dir       The directory to change to before running the command.
     * @return A two element string array, with stdout in element 1 and stderr in element 2.
     * @throws NullPointerException if the cmd argument is null.
     * @throws IOException if the cmd cannot be run.
     */
    public static String[] runCmd(String cmd, String[] env, File dir) throws NullPointerException, IOException
    {
        if ( cmd == null ) 
            throw new NullPointerException();

        String s = null;
        StringBuilder stdout = new StringBuilder("");
        StringBuilder stderr = new StringBuilder("");
        try {

            Process p = null;

            /* Choose the exec method. */
            if ( dir != null )
                p = Runtime.getRuntime().exec(cmd, env, dir);
            else if ( (dir == null) && (env != null) )
                p = Runtime.getRuntime().exec(cmd, env);
            else 
                p = Runtime.getRuntime().exec(cmd);

            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            while ((s = stdInput.readLine()) != null) {
                stdout.append(s);
                stdout.append("\n");
            }
            while ((s = stdError.readLine()) != null) {
                stderr.append(s);
                stderr.append("\n");
            }
            stdInput.close();
            stdError.close();

            int exitCode = p.waitFor();

            /* Closing and destroying should alleviate problems with files left open. */
            Closeable c = p.getOutputStream();
            c.close();
            p.destroy();
        }
        catch (IOException e) {
            log.error("Failed to run cmd: " + cmd, e);
            if ( (e.getMessage() != null) && (e.getMessage().length() > 0) )
                throw new IOException("Failed to run command: " + e.getMessage());
            else
                throw new IOException("Failed to run command: unknown cause.");
        }
        catch (InterruptedException ie) {
            log.error("Failed to run cmd: " + cmd, ie);
            if ( (ie.getMessage() != null) && (ie.getMessage().length() > 0) )
                throw new IOException("Failed to run command: " + ie.getMessage());
            else
                throw new IOException("Failed to run command: unknown cause.");
        }

        String[] out = { stdout.toString(), stderr.toString() };
        return out;
    }

    /**
     * Run a command and return the output.
     * @param cmd       The external command to run.
     * @return A two element string array, with stdout in element 1 and stderr in element 2.
     * @throws NullPointerException if the cmd argument is null.
     * @throws IOException if the cmd cannot be run.
     */
    public static String[] runCmd(String cmd) throws NullPointerException, IOException
    {
        return runCmd(cmd, null, null);
    }

    /**
     * Run a command from the specified directory (and possibly with altered environment) and return the output.
     * @param cmd       The external command to run along with and its arguments 
     * @param env       Environment variables in the form NAME=VALUE
     * @param dir       The directory to change to before running the command.
     * @return A two element string array, with stdout in element 1 and stderr in element 2.
     * @throws NullPointerException if the cmd argument is null.
     * @throws IOException if the cmd cannot be run.
     */
    public static String[] runCmd(String[] cmd, String[] env, File dir) throws NullPointerException, IOException
    {
        if ( cmd == null ) 
            throw new NullPointerException();

        String s = null;
        StringBuilder stdout = new StringBuilder("");
        StringBuilder stderr = new StringBuilder("");
        try {

            Process p = null;

            /* Choose the exec method. */
            if ( dir != null )
                p = Runtime.getRuntime().exec(cmd, env, dir);
            else if ( (dir == null) && (env != null) )
                p = Runtime.getRuntime().exec(cmd, env);
            else 
                p = Runtime.getRuntime().exec(cmd);

            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            while ((s = stdInput.readLine()) != null) {
                stdout.append(s);
                stdout.append("\n");
            }
            while ((s = stdError.readLine()) != null) {
                stderr.append(s);
                stderr.append("\n");
            }
            stdInput.close();
            stdError.close();

            int exitCode = p.waitFor();

            /* Closing and destroying should alleviate problems with files left open. */
            Closeable c = p.getOutputStream();
            c.close();
            p.destroy();
        }
        catch (IOException e) {
            log.error("Failed to run cmd: " + cmd, e);
            if ( (e.getMessage() != null) && (e.getMessage().length() > 0) )
                throw new IOException("Failed to run command: " + e.getMessage());
            else
                throw new IOException("Failed to run command: unknown cause.");
        }
        catch (InterruptedException ie) {
            log.error("Failed to run cmd: " + cmd, ie);
            if ( (ie.getMessage() != null) && (ie.getMessage().length() > 0) )
                throw new IOException("Failed to run command: " + ie.getMessage());
            else
                throw new IOException("Failed to run command: unknown cause.");
        }

        String[] out = { stdout.toString(), stderr.toString() };
        return out;
    }

    /**
     * Run a command and return the output.
     * @param cmd       The external command to run along with its arguments.
     * @return A two element string array, with stdout in element 1 and stderr in element 2.
     * @throws NullPointerException if the cmd argument is null.
     * @throws IOException if the cmd cannot be run.
     */
    public static String[] runCmd(String[] cmd) throws NullPointerException, IOException
    {
        return runCmd(cmd, null, null);
    }

    /**
     * Convert a byte array to a hex string
     * @param bytes     The byte array to convert.
     * @return A String object holding the hex representation of the bytes.
     */
    public static String bytesToHex( byte[] bytes )
    {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ )
        {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Concatenate two sets of strings.
     * @param first     The first array of strings.
     * @param second    The seconds array of strings.
     * @return A String[] array containing the first array followed by the second array.
     * If first is not null and second is null, return the first.
     * If first is null and second is not null, return the second.
     * If first is null and second is null, return null.
     */
    public static String[] concatStrings( String[] first, String[] second )
    {
        List<String> both;
        if ( (first == null) && (second == null) )
        {
            return null;
        }
        else if ( (first != null) && (second == null) )
        {
            both = new ArrayList<String>(first.length);
            Collections.addAll(both, first);
        }
        else if ( (first == null) && (second != null) )
        {
            both = new ArrayList<String>(second.length);
            Collections.addAll(both, second);
        }
        else 
        {
            both = new ArrayList<String>(first.length + second.length);
            Collections.addAll(both, first);
            Collections.addAll(both, second);
        }
        return both.toArray(new String[both.size()]);
    }
}
