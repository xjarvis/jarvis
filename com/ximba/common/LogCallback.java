package com.ximba.common;

// log4j
import org.apache.log4j.spi.LoggingEvent;

/**
 * <p> 
 * Class used to provide callbacks for classes that wish to be notified when log4j messages arrive. 
 * Used in conjunction with the LogAppender class.
 * </p>
 * @author Michael J. Hammel
 * @since 1.0
 */

public interface LogCallback {

    /**
     * Inbound string message from log4j.
     * @param event   The LoggingEvent from log4j.
     */
    public void append(LoggingEvent event);
}
