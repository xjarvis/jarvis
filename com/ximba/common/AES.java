package com.ximba.common;

// Java
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.log4j.Logger;

/**
 * <p> 
 * AES encryption support via static methods.
 * For an explanation as to why we use 128 instead of 256 (other than practical reasons
 * related to software requirements), see
 * http://security-architect.com/does-size-matter-aes-128-bit-encryption-is-probably-good-enough/
 * </p>
 * @author Michael J. Hammel
 * @since 1.0
 * @see https://howtodoinjava.com/security/java-aes-encryption-example/
 * @see https://stackoverflow.com/a/5760584 - Ordering of string processing for AES
 * @see https://stackoverflow.com/a/7797626 = Example of encrypt (nodejs) to decrypt (java)
 */

public class AES {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.common.AES");

    /* A generated secret key */
    private SecretKeySpec secretKey;

    /* An IV byte array */
    byte iv[] = new byte[16];

    /*
     * ---------------------------------------------------------------
     * Constructors
     * ---------------------------------------------------------------
     */
    public AES() { }

    /*
     * =======================================================
     * Private methods
     * =======================================================
     */

    /*
     * Generate a secret key from the password passed from the caller.
     * Remote end is NodeJS which uses MD5 to hash the key.
     */
    private void setKey(String password) throws Exception
    {
        MessageDigest md = null;
        byte[] key;
        byte[] digest;

        key = password.getBytes("UTF-8");
        md = MessageDigest.getInstance("MD5");
        digest = md.digest(key);
        // secretKey = new SecretKeySpec(key, "AES/CBC/PKCS5Padding");
        secretKey = new SecretKeySpec(key, "AES");
    }

    /*
     * Generate an IV byte array.
     * This is like RandomGUID, but less sophisticated.
     */
    private IvParameterSpec genIV() 
    {

        /* Generate an IV byte array */
        SecureRandom random = new SecureRandom();
        random.nextBytes(iv);
        return new IvParameterSpec(iv);
    }

    /*
     * Convert Base64 encoded string to an IvParameterSpec.
     */
    private IvParameterSpec toIV(String ivStr) 
    {
        iv = Base64.getDecoder().decode(ivStr.getBytes());
        log.info("iv length: " + iv.length);
        return new IvParameterSpec(iv);
    }

    /*
     * =======================================================
     * Public methods
     * =======================================================
     */

    /*
     * Encrypt a message.
     * 1. Get message String as bytes
     * 3. Encrypt
     * 2. Endcode Base64 bytes
     * 4. Convert to a String and return.
     * @param message   The message to encrypt.
     * @param cryptkey  The encryption key.
     * @return A String object that is the Base64 encoded version of the encrypted message or null on error.
     * Use getIV() to retrieve the IV string used to encode the message.
     */
    public String encrypt(String message, String cryptkey) throws Exception
    {
        String password = cryptkey;

        /* password must be 16 bytes for 128bit encryption */
        if ( password.length() < 16 )
        {
            throw new Exception("Password must be at least 16 bytes, with only the first 16 significant.");
        }
        if ( password.length() > 16 )
        {
            password = password.substring(0,16);
        }
        log.info("password: " + password);

        try 
        {
            setKey(password);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, genIV());
            byte[] data = cipher.doFinal(message.getBytes());
            String messageStr = Base64.getEncoder().encodeToString(data);
            return messageStr;
        } 
        catch (Exception e) 
        {
            log.error("Error while encrypting: " + e.toString());
        }
        return null;
    }

    /*
     * Decrypt a message.
     * This is the reverse of the remote end process, as so:
     * 1. Get message String as bytes
     * 2. Decode Base64 bytes
     * 3. Decrypt
     * 4. Convert decrypted bytes back to a String and return.
     * @param message   The message to decrypt.
     * @param cryptkey  The encryption key (re: our UUID).
     * @param iv        The initialization vector as a Base64 encoded string.
     * @return A String object that contains the unencoded message or null on error.
     */
    public String decrypt(String message, String cryptkey, String iv) throws Exception
    {
        String password = cryptkey;

        /* password must be 16 bytes for 128bit encryption */
        if ( password.length() < 16 )
        {
            throw new Exception("Password must be at least 16 bytes, with only the first 16 significant.");
        }
        if ( password.length() > 16 )
        {
            password = password.substring(0,16);
        }

        try 
        {
            setKey(password);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKey, toIV(iv));
            byte[] messageBytes = Base64.getDecoder().decode( message.getBytes() );
            byte[] data = cipher.doFinal(messageBytes);
            return new String(data);
        } 
        catch (Exception e) 
        {
            log.error("Error while decrypting: " + e.toString() );
            e.printStackTrace();
        }
        return null;
    }

    /*
     * Retrieve the IV as a Base64 encoded string.
     * @return A String object that contains the Base64 encoded IV bytes or null 
     * if the IV has not yet been generated.
     */
    public String getIV()
    {
        return Base64.getEncoder().encodeToString(iv);
    }
}
