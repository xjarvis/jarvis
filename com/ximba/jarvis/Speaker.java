package com.ximba.jarvis;

// Java
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.log4j.Logger;

// Sound and Audio
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import sun.audio.*;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

// Jarvis
import com.ximba.common.*;

/**
 * <p> 
 * Text to Speach module for Jarvis.
 * </p>
 * @author Michael J. Hammel
 * @since 1.0
 */

public class Speaker implements Runnable {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.jarvis.Speaker");

    /** Message queue - the list of files to process. */
    private static final ConcurrentLinkedQueue<Message> msgs = new ConcurrentLinkedQueue<Message>();

    /*
     * Thread Managment
     */

    /* This thread. */
    private Thread thread = null;

    /* When true, the thread is running. */
    private boolean isRunning = false;

    /* When true, the thread has completed and is ready to change state to "not running". */
    private boolean runComplete = false;

    /* When shutting down, disable exception hanlding in run() method. */
    private boolean doShutdown = false;

    /* Speech class */
    Speech speech = null;

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    void Speaker() { }

    /*
     * =======================================================
     * Inner classes
     * =======================================================
     */

    /*
     * =======================================================
     * Private methods
     * =======================================================
     */

    /** 
     * Handle inbound messages.
     */
    private void process(Message msg)
    {
        File file;
        String filename;
        String messageFilename;
        String audioFilename;

        if ( msg == null )
            return;

        // Pull the text to speak from message
        log.info("Numer of phrases to speak: " + msg.getDataSize());
        Map<String, String> phrases = msg.getData();

        // Sorted keys from the phrases
        ArrayList<String> sortedKeys = new ArrayList<String>(phrases.keySet());
        Collections.sort(sortedKeys);

        /* Disable speech processing while Jarvis speaks. */
        log.info("Disabling speech processing so Jarvis can talk.");
        speech.disable();

        for (String key : sortedKeys)
        {
            String phrase = (String)phrases.get(key);
            log.info(key + " = " + phrase);

            if ( ! key.matches("(^)"+Message.S_SPEAK+"(.*)") )
            {
                log.warn("Skipping phrase: " + phrase);
                continue;
            }

            filename = Cli.get(Cli.S_RUNDATADIR) + File.separator + new RandomGUID().toString();
            messageFilename = filename + "-a.wav";
            audioFilename = filename + "-b.wav";
    
            /*
             * Write text to file. It's easier to pass that as input to the espeak command.
             */
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(messageFilename));
                writer.write(phrase);
            }
            catch (Exception e) {
                log.error("Exception while trying to save text to speak.", e);
            }
            finally {
                try { writer.close(); }
                catch (IOException ioe) {}
            }
    
            try {
                
            /*
             * The old way - via Google Translate API, which doesn't work anymore
             *
                // Build a request string to Google's Speech API
                // We always ask for english
                String request = 
                    "http://www.translate.google.com/translate_tts?ie=UTF-8" +
                    "&q=" + phrase.replace(" ","%20") +
                    "&tl=en-us";
    
                // Setup and send the request to Google
                URL url = new URL(request); 
                URLConnection connection = url.openConnection();           
                connection.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
    
                // Play the mp3
                InputStream inStream = connection.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(inStream);
                try { new Player(bis).play(); }
                catch(Exception e) { 
                    log.error("Failed to play MP3.");
                }
    
                // Cleanup
                bis.close();
                inStream.close();
             */
    
                /*
                 * The new way:  Use espeak-ng.  The voice is not so great but it's easier than AWS or other serivces.
                 * See: https://stackoverflow.com/a/12059638
                 * See: man espeak-ng
                 */
                String cmd = "espeak-ng -a 160 -g0.025 -k20 -l1 -p 80 -s 150 -v mb-us1 -f " + 
                                messageFilename + " -w " + audioFilename;
                log.info("Running espeak cmd: " + cmd);
                Utils.runCmd(cmd);
    
                file = new File(audioFilename);
                if ( file.exists() )
                {
                    log.info("Found wav file: " + audioFilename + ", attempting to play.");
    
                    /* 
                     * Now play the generated audio.
                     * But try to wait for it to be completed. This may be longer than 
                     * the clip length but sync'ing turns out to be hard in Java on Linux.
                     */
                    try
                    {
                        /* Get the length of the clip. */
                        Clip clip = AudioSystem.getClip();
                        clip.open(AudioSystem.getAudioInputStream(file));
                        long delay = (long)((clip.getMicrosecondLength() / 1000)*2);
                        clip.close();

                        /* Play it in the background. */
                        InputStream in = new FileInputStream(audioFilename);
                        AudioStream audioStream = new AudioStream(in);
                        AudioPlayer.player.start(audioStream);

                        /* And delay for the clip length before moving on. */
                        Thread.sleep(delay);
                    }
                    catch (Exception exc)
                    {
                        exc.printStackTrace(System.out);
                    }
    
                    /* And cleanup */
                    file.delete();

                    /* Pause briefly between phrases */
                    try { Thread.sleep(1000); }
                    catch(Exception e){}
                }
                else
                {
                    log.info("Wav file not found.");
                }
            }
            catch(Exception e){
                log.error("Exception while trying to play audio.", e);
            }

            file = new File(messageFilename);
            if ( file.exists() )
                file.delete();
        }

        /* Re-enable speech processing. */
        log.info("Enabling speech processing after Jarvis has spoken.");
        speech.enable();
    }

    /*
     * =======================================================
     * Public Methods - Thread Managmenet
     * =======================================================
     */

    /** 
     * Submit a message to this thread.
     * The queue is thread-safe.
     */
    public static void submit(Message msg) 
    {
        synchronized (msgs)
        {
            if ( msg != null )
                msgs.add( msg );
        }
    }

    /** 
     * Setup specific setup items.
     */
    boolean setup(Speech speech) 
    {
        /* We need this so we can disable speech input while Jarvis speaks. */
        this.speech = speech;

        if ( !isRunning )
        {
            return true;
        }
        return false;
    }

    /** 
     * Start the thread.
     * @return True if the thread is started, false if it is already running.
     * @throws IOException if the thread cannot start.
     */
    synchronized boolean start() throws IOException
    {
        /*
         * Create and start the thread.
         */
        if ( !isRunning )
        {
            // Clear the inbound queue when we start, since its a class variable.
            synchronized (msgs)
            {
                msgs.clear();
            }

            this.thread = new Thread(this, "SPEAKR");
            thread.start();

            /* Wait for thread to start. */
            Calendar startWait = Calendar.getInstance();
            while ( !isRunning )
            {
                Calendar now = Calendar.getInstance();
                now.add(Calendar.SECOND, (-1*5));
                if ( now.after(startWait) )
                    break;
            }
            if ( !isRunning )
            {
                thread.interrupt();
                thread = null;
                runComplete = false;
                throw new IOException("Timed out waiting for server thread to start.");
            }
            return true;
        }
        else
            return false;
    }

    /** 
     * Stop the thread.  This always succeeds and can be used to make
     * sure the thread is ready to be started again.
     */
    synchronized void shutdown()
    {
        doShutdown = true;
        if ( isRunning )
        {
            isRunning = false;
            thread.interrupt();

            /* Wait for the thread to stop. */
            Calendar startWait = Calendar.getInstance();
            while ( !runComplete )
            {
                Calendar now = Calendar.getInstance();
                now.add(Calendar.SECOND, (-1*5));
                if ( now.after(startWait) )
                    break;
            }
            log.info("Speaker shutdown complete.");
        }
        doShutdown = false;
    }

    /** 
     * Handle inbound messages.
     */
    public void run()
    {
        if ( isRunning )
            return;
        runComplete = false;

        /*
         *----------------------------------------------------------------
         * Spin, waiting on shutdown.
         *----------------------------------------------------------------
         */
        log.info("Starting Speaker Thread.");
        isRunning = true;
        while ( isRunning )
        {
            Message msg = null;
            synchronized( msgs )
            {
                msg = msgs.poll();
            }
            if ( msg != null )
            {
                process(msg);
            }
            else
            {
                try { Thread.sleep(0,100000); }
                catch(Exception e){}
            }
        }
        runComplete = true;
        log.info("Speaker thread exiting.");
    }
}
