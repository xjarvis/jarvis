package com.ximba.jarvis;

/* Java */
import java.util.*;
import java.io.*;
import java.lang.*;

// OpenNLP
import opennlp.tools.namefind.*;
import opennlp.tools.postag.*;
import opennlp.tools.tokenize.*;
import opennlp.tools.util.*;

/**
 * <p> Native Language Processing for a command. </p>
 * @author Michael J. Hammel
 * @since 1.0
 */
public class NLP {

    // Bin files are from Apache OpenNLP distribution.
    private static final String S_TOKENBIN = "en-token.bin";
    private static final String S_POSBIN   = "en-pos-maxent.bin";
    private static final String S_NAMEBIN  = "en-ner-person.bin";

    // The command to process.
    private String cmd = null;

    // The tokenized command
    String[] tokens = null;

    // Spans definining the names within the command.
    Span[] nameSpans = null;

    // Parts of speech tags and their probabilities
    String[] tags  = null;
    double[] probs = null;

    /*
     * ---------------------------------------------------------------
     * Constructors
     * ---------------------------------------------------------------
     */

    /**
     * Constructor requires a string command to parse.
     * @param cmd    The string command to parse.
     * @throws Exception if cmd is null.
     */
    public NLP( String cmd ) throws Exception {
        if ( cmd == null )
            throw new Exception( "Missing command." );
        this.cmd = cmd;
    }

    /*
     * ---------------------------------------------------------------
     * Private methods
     * ---------------------------------------------------------------
     */

    /** 
     * Retrieve specific tags.
     * @return An array of strings that match the specified tag.
     * @note see http://blog.dpdearing.com/2011/12/opennlp-part-of-speech-pos-tags-penn-english-treebank/
     */
    private String[] getTags(String tag) { 
        if ( tags == null )
            return null;

        List<String> results = new ArrayList<String>();
        synchronized ( cmd ) 
        {
            for(int i=0; i<tags.length; i++)
            {
                if ( tags[i].startsWith(tag.toUpperCase()) )
                    results.add( new String( tokens[i] ) );
            }
        }
        String[] data = null;
        if ( results.size() > 0 )
            data = results.toArray(new String[0]);
        return data;
    }

    /*
     * ---------------------------------------------------------------
     * Public methods
     * ---------------------------------------------------------------
     */

    /** 
     * Tokenize the string.
     * We only support english at the moment.
     */
    public void tokenize() throws Exception { 
        String bin = Cli.get(Cli.S_RUNDATADIR) + File.separator + S_TOKENBIN;
        InputStream modelIn = null;
        TokenizerModel model = null;
        try {
            modelIn = new FileInputStream( bin );
            model = new TokenizerModel( modelIn );
        }
        catch(IOException e) {
            throw new Exception( "Can't find bin: " + bin );
        }
        finally {
            if (modelIn != null) {
                try {
                    modelIn.close();
                }
                catch(IOException ioe) {
                }
            }
        }

        Tokenizer tokenizer = new TokenizerME( model );
        synchronized (cmd)
        {
            tokens = tokenizer.tokenize( cmd );
        }
    }

    /** 
     * Generate the parts of speech and their probabilities.
     * @note Requires the tokenizer() method be called first.
     * @see http://blog.dpdearing.com/2011/12/opennlp-part-of-speech-pos-tags-penn-english-treebank/
     */
    public void pos() throws Exception { 
        if ( tokens == null )
            throw new Exception( "Missing tokens." );

        String bin = Cli.get(Cli.S_RUNDATADIR) + File.separator + S_POSBIN;
        InputStream modelIn = null;
        POSModel model = null;
        try {
            modelIn = new FileInputStream( bin );
            model = new POSModel( modelIn );
        }
        catch(IOException e) {
            throw new Exception( "Can't find bin: " + bin );
        }
        finally {
            if (modelIn != null) {
                try {
                    modelIn.close();
                }
                catch(IOException ioe) {
                }
            }
        }

        POSTaggerME tagger = new POSTaggerME( model );
        synchronized (cmd)
        {
            tags = tagger.tag( tokens );
            probs = tagger.probs();
        }
    }

    /** 
     * Find names within the text.
     * @note Requires the tokenizer() method be called first.
     */
    public void names() throws Exception { 
        if ( tokens == null )
            throw new Exception( "Missing tokens." );

        String bin = Cli.get(Cli.S_RUNDATADIR) + File.separator + S_NAMEBIN;
        InputStream modelIn = null;
        TokenNameFinderModel model = null;
        try {
            modelIn = new FileInputStream( bin );
            model = new TokenNameFinderModel( modelIn );
        }
        catch(IOException e) {
            throw new Exception( "Can't find bin: " + bin );
        }
        finally {
            if (modelIn != null) {
                try {
                    modelIn.close();
                }
                catch(IOException ioe) {
                }
            }
        }

        NameFinderME nameFinder = new NameFinderME( model );
        synchronized (cmd)
        {
            nameSpans = nameFinder.find( tokens );
        }
    }

    /*
     * ---------------------------------------------------------------
     * Getters
     * ---------------------------------------------------------------
     */

    /** 
     * Retrieve the original command.
     * @return A string.
     */
    public String getCmd() { return cmd; }

    /** 
     * Retrieve tokens parsed from command
     * @return An array of Strings, ordered by position in the original command string.
     */
    public String[] getTokens() { 
        String[] data = null;
        synchronized ( cmd ) 
        {
            data = new String[tokens.length];
            for(int i=0; i<tokens.length; i++)
                data[i] = new String( tokens[i] );
        }
        return data; 
    }

    /** 
     * Retrieve any adjectives.
     * @return An array of adjectives, or null if no adjectives found.
     */
    public String[] getAdj() { 
        return getTags("JJ");
    }

    /** 
     * Retrieve any nouns.
     * @return An array of nouns, or null of no nouns found.
     */
    public String[] getNoun() { 
        return getTags("NN");
    }

    /** 
     * Retrieve any adverbs.
     * @return An array of adverbs, or null of no adverbs found.
     */
    public String[] getAdverbs() { 
        return getTags("RB");
    }

    /** 
     * Retrieve any verbs.
     * @return An array of verbs, or null of no verbs found.
     */
    public String[] getVerbs() { 
        return getTags("VB");
    }

    /** 
     * Retrieve any pronounds.
     * @return An array of pronouns, or null of no pronouns found.
     */
    public String[] getPronouns() { 
        return getTags("PR");
    }

    /** 
     * Retrieve all tags, so we can get positional args.
     * @return The list of tags for each token.
     */
    public String[] getTags() { 
        return tags;
    }

    /*
     * Retrieve all the names found in the command.
     * @return A String array of names, or NULL if there are no names found.
     */
    public String[] getNames() { 
        if ( nameSpans != null )
        {
            return Span.spansToStrings(nameSpans, tokens);
        }
        return(null);
    }

    /**
     * Data dump of this NLP analysis.
     */
    public String toString() {
        StringBuilder str = new StringBuilder("NLP Analysis:\n");

        // Append the command
        str.append("Command: " + cmd);
        str.append("\n");

        // Append tokens, if any.
        if ( tokens != null )
        {
            str.append( "Tokens: " );
            for(int i=0; i<tokens.length; i++)
                str.append( tokens[i] + " " );
            str.append( "\n" );
        }

        // Append names, if any.
        if ( nameSpans != null )
        {
            String[] names = Span.spansToStrings(nameSpans, tokens);
            str.append( "Names: \n" );
            for(int i=0; i<names.length; i++)
            {
                str.append( "    " + names[i] + "\n" );
            }
            str.append( "\n" );
        }

        // Append parts of speech, if any.
        if ( tags != null )
        {
            str.append( "POS: \n" );
            for(int i=0; i<tags.length; i++)
            {
                str.append( String.format("    (%5.2f) %s %s\n", probs[i], tags[i], tokens[i]) );
            }
            str.append( "\n" );
        }

        return str.toString();
    }

}
