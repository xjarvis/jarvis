package com.ximba.jarvis;

/* Java */
import java.io.*;
import java.lang.*;
import java.net.*;
import java.nio.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.security.*;
import java.util.*;
import org.apache.log4j.Logger;

/* Crypto */
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

// JSON-Simple
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

// Jarvis
import com.ximba.common.*;

/**
 * <p> Submit requests to Ironman monitors via imwww REST API and get responses, if any. </p>
 * @author Michael J. Hammel
 * @since 1.0
 */
public class Imwww {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.jarvis.Imwww");

    /* Ports used on the monitor */
    private static int HTTP_PORT  = 8165;

    /* Version of monitor which we support */
    private static String monitorVersion = "1.0";

    /* Registration object: we set this once. */
    private static Register register = null;

    /* Internet address of the monitor to send to, if any. */
    private InetAddress addr = null;

    /*
     * ---------------------------------------------------------------
     * Constructors
     * ---------------------------------------------------------------
     */
    public Imwww() { }

    /*
     * =======================================================
     * Private methods
     * =======================================================
     */

    /*
     * =======================================================
     * Public methods
     * =======================================================
     */

    /** 
     * Store the registration object.
     */
    void setRegister( Register register )
    {
        this.register = register;
    }

    /** 
     * If set, we only send to this address.
     */
    void setAddress( InetAddress addr )
    {
        this.addr = addr;
    }

    /** 
     * Clear a previously set monitor address, if any.
     * This allows reused of a single instance of this class for multiple
     * messages.
     */
    void clearAddress()
    {
        this.addr = null;
    }

    /*
     * Make REST API call.
     * Return a Message object.
     */
    public Message send( String method, String uri, String json ) throws Exception
    {
        if ( register == null )
        {
            log.error("No registration object. Can't send message to monitors.");
            return null;
        }

        List<Monitor> monitors = register.getMonitors();
        if ( monitors.size() == 0 )
        {
            log.error("No monitors registered. Message not sent.");
            return null;
        }

        /*
         * Iterate over all monitors and send to them.
         */
        Message msg = new Message();
        for ( Monitor monitor: monitors )
        {
            String scheme;
            String request;
            int port;
            InetAddress addr = monitor.getAddress();

            /* If we're sending to a specific end point, check this monitor matches it. */
            if ( (this.addr != null) && (!monitor.matches( addr )) )
            {
                log.info("Skip sending to monitor @ " + this.addr.getHostAddress());
                continue;
            }

            /* Build the request. */
            request = "http://" + addr.getHostAddress() + ":" + HTTP_PORT + uri;
            log.info("Trying to contact URL: " + request);

            /* Setup the connection to monitor */
            URL url = new URL(request); 
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();           
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(false); 
            connection.setRequestMethod(method); 
            connection.setRequestProperty("User-Agent", "jarvis"); 
            connection.setRequestProperty("Accept-Version", monitorVersion); 
            connection.setConnectTimeout(60000);
            connection.setUseCaches (false);
            if ( json != null )
            {
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8"); 
            }
        
            /* Submit the request. */
            if ( json != null )
            {
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
                wr.write( json.getBytes("UTF-8") );
                wr.flush();
                wr.close();
            }

            /* Check if the remote server is happy with our request. */
            if ( connection.getResponseCode() != 200 )
            {
                throw new Exception("HTTP error: " + connection.getResponseCode() );
            }
            log.info("Messaged monitor @ " + addr.getHostAddress() );

            /* Pull the response, if any */
            StringBuilder response = new StringBuilder("");
            BufferedReader in = new BufferedReader( new InputStreamReader(connection.getInputStream()) );
            String decodedString;
            while ((decodedString = in.readLine()) != null) {
                response.append(decodedString + "\n");
            }

            /* Close the connection */
            connection.disconnect();
           
            if ( response.toString().length() == 0 )
            {
                log.error("No data in response from monitor @ " + addr.getHostAddress() + ".  Skipping.");
                continue;
            }
            log.info("Response: " + response.toString());
    
            /* Shove the response into a Message object for passing back to the caller. */
            msg.addData( addr.getHostAddress(), response.toString() );
        }
        return msg;
    }

    /*
     * Encode data with AES and an IV string.  Then return the encoded data and IV string
     * as a JSON-encoded string.
     * @param message  The string to encode.
     * @see http://www.novixys.com/blog/java-aes-example/
     * @see https://howtodoinjava.com/security/java-aes-encryption-example/
     */
    @SuppressWarnings("unchecked")
    public String encode( String message ) throws Exception
    {
        AES aes = new AES();
        String encryptedMsg = aes.encrypt(message, Register.getUUID());

        /* build json data. */
        JSONObject json = new JSONObject();
        log.info("iv: " + aes.getIV());
        log.info("message: " + encryptedMsg);
        json.put("iv", aes.getIV());
        json.put("message", encryptedMsg);

        /* Return the string */
        return (json.toJSONString());

    }

    /*
     * Issue a state change request to a remote device.
     * @param monitor  The monitor to contact.
     * @param uuid     The device to contact.
     * @param action   The state change to apply.
     */
    @SuppressWarnings("unchecked")
    public void setDevice( Monitor monitor, String uuid, String action )
    {
        /* Build JSON object to send */
        JSONObject json = new JSONObject();
        json.put("command", "update");
        json.put("uuid", uuid);
        json.put("state", action);

        /* Make the API call: We don't need the response. */
        try {
            String encodedJson = encode(json.toString());
            setAddress(monitor.getAddress());
            send("POST", "/set/device", encodedJson);
        }
        catch(Exception e) {
            log.error("Failed /set/devices request: " + e.getMessage());
        }
    }
}
