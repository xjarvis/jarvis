package com.ximba.jarvis;

/* Java */
import java.util.*;
import java.net.*;
import java.io.*;
import java.lang.*;
import org.apache.log4j.Logger;

// JSON-Simple
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

// Jarvis
import com.ximba.common.*;

/**
 * <p> Initiate and manage registrations with Ironman monitors. </p>
 * @author Michael J. Hammel
 * @since 1.0
 */
public class Register {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.jarvis.Register");
    private static int AckHandlerPort = 4445;
    private static String MULTICAST_IP = new String("234.1.42.3");
    private static int MULTICAST_PORT = 13911;

    /* Type and Action bytes of multicast message to piboxd */
    private static byte MT_IRONMAN = 0x07;
    private static byte MT_PAIR_JARVIS = 0x02;

    /* Instance variables */
    // private InetAddress     address = null;    // Address of remote monitor
    // private String          descriptor = null; // Descriptor of the monitor

    /* Monitor queue - the list of Monitors we can talk to. */
    private static final List<Monitor> monitors = new ArrayList<Monitor>();

    /* Our UUID for this instance of Jarvis.  It changes on each run. */
    private static final String uuid = new RandomGUID().toString();

    /* Registration state. */
    private boolean enabled = true;

    /* Inner class instances */
    private AckHandler ackHandler = null;
    private Ping ping = null;

    /*
     * ---------------------------------------------------------------
     * Constructors
     * ---------------------------------------------------------------
     */

    public Register() {}

    /*
     * ---------------------------------------------------------------
     * Inner classes
     * ---------------------------------------------------------------
     */

    /**
     * Class: AckHandler
     * Responsible for processing inbound ACK responses from Ping class
     * multicast messages.
     */
    private class AckHandler implements Runnable {

        private Thread thread = null;
        private boolean isRunning = false;
        private boolean runComplete = false;
        private boolean doShutdown = false;
        private Register register = null;

        /**
         * Constructor
         */
        public AckHandler ( Register register )
        {
            this.register = register;
        }

        /**
         * Handle inbound messages.
         */
        private void process(InetAddress addr, String msg)
        {
            // Check if monitor is already registered
            for ( Monitor monitor: monitors )
            {
                log.info("Looking for monitor @ " + addr);
                if ( monitor.matches( addr ) )
                {
                    log.warn("Monitor @ " + addr.getHostAddress() + " is already registered.");
                    return;
                }
            }

            /*
             * Save the monitor.  If we don't register properly for some reason we'll
             * take it back out.
             */
            Monitor monitor = new Monitor();
            monitor.register( addr, msg );
            monitors.add(monitor);

            /*
             * If we get here then try to pair with the server, then
             * create a new monitor entry.
             * Monitors return their descritor in the payload of the ACK.
             */
            Message response = null;
            Imwww imwww = new Imwww();
            imwww.setRegister(register);
            try {
                response = imwww.send("POST", "/pair/jarvis/" + uuid, null);
            }
            catch(Exception e) {
                log.error("Failed to pair with monitor @ " + addr.getHostAddress());
                log.error("Reason: " + e.getMessage());

                /* We need to remove this if we didn't complete the registration. */
                monitors.remove(monitor);
                return;
            }

            log.info("Registered monitor @ " + addr.getHostAddress() );
            if ( response.getDataSize() > 0 )
            {
                log.info("Response: " + response.getDataItem(addr.getHostAddress()));
            }

            /* Get descriptor. */
            try {
                imwww.setAddress( addr );
                response = imwww.send("GET", "/monitor", null);
            }
            catch(Exception e) {
                log.error("Failed to get description for monitor @ " + addr.getHostAddress());
                log.error("Reason: " + e.getMessage());
                monitor.setDescription( "No description" );
                return;
            }

            if ( response.getDataSize() == 0 )
            {
                log.error("No descriptor from: " + addr.getHostAddress());
                monitor.setDescription( "No description" );
                return;
            }

            /* Response is JSON with AES encrypted description. */
            String description = response.decode( addr.getHostAddress(), register );
            monitor.setDescription( description );
            log.info("Description: " + description );

            /*
            try {
                JSONParser parser=new JSONParser();
                JSONObject jsonObject=(JSONObject)parser.parse(response.getDataItem(addr.getHostAddress()));
                String iv      = (String) jsonObject.get("iv");
                String message = (String) jsonObject.get("message");

                if ( (iv==null) || (message==null) )
                {
                    log.error("Incomplete response to /monitor.  No description for monitor @ " + addr.getHostAddress());
                    return;
                }

                AES aes = new AES();
                String description = aes.decrypt(message, Register.getUUID(), iv);
                monitor.setDescription( description );
                log.info("Description: " + description );
            }
            catch(Exception e){
                log.error("Invalid response to /monitor.  No description for monitor @ " + addr.getHostAddress());
                return;
            }
            */
        }

        /**
         * Start the thread.
         * @return True if the thread is started, false if it is already running.
         * @throws IOException if the thread cannot start.
         */
        synchronized boolean start() throws IOException
        {
            /*
             * Create and start the thread.
             */
            if ( !isRunning )
            {
                this.thread = new Thread(this, "AckHandler");
                thread.start();

                /* Wait for thread to start. */
                Calendar startWait = Calendar.getInstance();
                while ( !isRunning )
                {
                    Calendar now = Calendar.getInstance();
                    now.add(Calendar.SECOND, (-1*5));
                    if ( now.after(startWait) )
                        break;
                }
                if ( !isRunning )
                {
                    thread.interrupt();
                    thread = null;
                    runComplete = false;
                    throw new IOException("Timed out waiting for server thread to start.");
                }
                return true;
            }
            else
                return false;
        }

        /**
         * Stop the thread.  This always succeeds and can be used to make
         * sure the thread is ready to be started again.
         */
        synchronized void shutdown()
        {
            doShutdown = true;
            if ( isRunning )
            {
                isRunning = false;
                thread.interrupt();

                /* Wait for the thread to stop. */
                Calendar startWait = Calendar.getInstance();
                while ( !runComplete )
                {
                    Calendar now = Calendar.getInstance();
                    now.add(Calendar.SECOND, (-1*5));
                    if ( now.after(startWait) )
                        break;
                }
                log.info("AckHandler shutdown complete.");
            }
            doShutdown = false;
        }

        /**
         * Handle inbound messages.
         */
        public void run()
        {
            byte buf[] = new byte[256];

            if ( isRunning )
                return;

            log.info("Starting AckHandler Thread.");
            isRunning = true;
            runComplete = false;

            int retry = 0;
            DatagramSocket socket = null;
            while ( (socket == null) && (retry < 25) )
            {
                try {
                    socket = new DatagramSocket(AckHandlerPort);
                    socket.setReuseAddress(true);
                    socket.setSoTimeout(250);
                }
                catch( SocketException se )
                {
                    log.error("Failed to create datagram socket: " + se.getMessage());
                    log.error("Trying again.");
                    try { Thread.sleep(5000); }
                    catch(Exception e){}
                    continue;
                }
            }
            if ( retry == 25 )
            {
                log.error("Couldn't get socket for registration!!");
                return;
            }

            /*
             *----------------------------------------------------------------
             * Spin, waiting on shutdown.
             *----------------------------------------------------------------
             */
            while ( isRunning )
            {
                if ( enabled )
                {
                    boolean skip = false;
                    DatagramPacket packet = null;
                    try {
                        packet = new DatagramPacket(buf, buf.length);
                        socket.receive(packet);
                    }
                    catch( SocketTimeoutException stoe )
                    {
                        skip = true;
                    }
                    catch( SocketException se )
                    {
                        log.error("Failed to create datagram packet: " + se.getMessage());
                        skip = true;
                    }
                    catch( IOException ioe )
                    {
                        log.error("Failed to receive datagram packet: " + ioe.getMessage());
                        skip = true;
                    }
                    if ( !skip )
                    {
                        /* Packet received.  Process it! */
                        log.info("ACK packet received.");
                        String received = new String(packet.getData(), 0, packet.getLength());
                        process(packet.getAddress(), received);
                    }
                }
                try { Thread.sleep(0,250); }
                catch(Exception e){}
            }
            if ( socket != null )
            {
                socket.close();
            }
            runComplete = true;
            log.info("AckHandler thread exiting.");
        }
    }

    /**
     * Ping Class: used to ask monitors to respond.
     */
    private class Ping implements Runnable {

        /*
         * Thread Managment
         */

        /* This thread. */
        private Thread thread = null;

        /* When true, the thread is running. */
        private boolean isRunning = false;

        /* When true, the thread has completed and is ready to change state to "not running". */
        private boolean runComplete = false;

        /* When shutting down, disable exception hanlding in run() method. */
        private boolean doShutdown = false;

        /**
         * Send client multicast message (not broadcast! Piboxd already supports multicast!)
         */
        private void process(byte[] msg )
        {
            DatagramSocket socket;
            InetAddress group;
            try {
                socket = new DatagramSocket();
                group = InetAddress.getByName(MULTICAST_IP);

                DatagramPacket packet = new DatagramPacket(msg, msg.length, group, MULTICAST_PORT);
                socket.send(packet);
                socket.close();
                log.info("Ping packet sent: " + Utils.bytesToHex(msg) );
            }
            catch(Exception e) {
                log.error("Failed to send Ping packet: " + e.getMessage());
            }
        }

        /**
         * Start the thread.
         * @return True if the thread is started, false if it is already running.
         * @throws IOException if the thread cannot start.
         */
        synchronized boolean start() throws IOException
        {
            /*
             * Create and start the thread.
             */
            if ( !isRunning )
            {
                this.thread = new Thread(this, "Ping");
                thread.start();

                /* Wait for thread to start. */
                Calendar startWait = Calendar.getInstance();
                while ( !isRunning )
                {
                    Calendar now = Calendar.getInstance();
                    now.add(Calendar.SECOND, (-1*5));
                    if ( now.after(startWait) )
                        break;
                }
                if ( !isRunning )
                {
                    thread.interrupt();
                    thread = null;
                    runComplete = false;
                    throw new IOException("Timed out waiting for server thread to start.");
                }
                return true;
            }
            else
                return false;
        }

        /**
         * Stop the thread.  This always succeeds and can be used to make
         * sure the thread is ready to be started again.
         */
        synchronized void shutdown()
        {
            doShutdown = true;
            if ( isRunning )
            {
                isRunning = false;
                thread.interrupt();

                /* Wait for the thread to stop. */
                Calendar startWait = Calendar.getInstance();
                while ( !runComplete )
                {
                    Calendar now = Calendar.getInstance();
                    now.add(Calendar.SECOND, (-1*5));
                    if ( now.after(startWait) )
                        break;
                }
                log.info("Ping shutdown complete.");
            }
            doShutdown = false;
        }

        /**
         * Loop repeatedly (until stopped) to send ping requests.
         */
        public void run()
        {
            if ( isRunning )
                return;

            log.info("Starting Ping Thread.");
            isRunning = true;
            runComplete = false;

            byte[] msg = new byte[12];
            int port;
            int size;

            /*
             * "msg" is the packet we send.  It's format is:
             * Byte 0:      Message type  (MT_IRONMAN)
             * Byte 1:      Action type (MA_PAIR_JARVIS)
             * Byte 3-4:    Unused
             * Byte 5-8:    Integer size of payload (payload is 4 bytes long)
             * Byte 9-12:   Payload (The integer port number Ack server uses)
             *
             * The receiving monitor then ACKs to this systems IP at the specified
             * port, which is where the AckHandler is listening.
             */
            msg[0] = MT_IRONMAN;
            msg[1] = MT_PAIR_JARVIS;
            msg[2] = 0;
            msg[3] = 0;
            size = 4;
            port = AckHandlerPort;
            for(int i = 0; i < 4; ++i)
            {
                msg[4+i] = (byte)(size & 0xFF);
                size >>= 8;
            }
            for(int i = 0; i < 4; ++i)
            {
                msg[8+i] = (byte)(port & 0xFF);
                port >>= 8;
            }

            /*
             *----------------------------------------------------------------
             * Spin, waiting on shutdown.
             *----------------------------------------------------------------
             */
            while ( isRunning )
            {
                if ( enabled )
                {
                    process(msg);
                }
                try { Thread.sleep(5000); }
                catch(Exception e){}
            }
            runComplete = true;
            log.info("Ping thread exiting.");
        }
    }

    /*
     * ---------------------------------------------------------------
     * Private methods
     * ---------------------------------------------------------------
     */

    /*
     * ---------------------------------------------------------------
     * Public Methods
     * ---------------------------------------------------------------
     */

    /**
     * Start the various threads used by the Registration object.
     * This includes:
     *      AckHandler      Process inbound ACK's from monitors
     *      Ping            Send broadcast Pings.
     */
    public void start()
    {
        // start AckHandler Thread
        if ( ackHandler == null )
        {
            ackHandler = new AckHandler(this);
            try { ackHandler.start(); }
            catch(Exception e){}
        }

        // start Ping Thread
        if ( ping == null )
        {
            ping = new Ping();
            try { ping.start(); }
            catch(Exception e){}
        }
    }

    public void stop()
    {
        // stop AckHandler Thread
        if ( ackHandler != null )
        {
            ackHandler.shutdown();
        }

        // stop Ping Thread
        if ( ping != null )
        {
            ping.shutdown();
        }
    }

    /**
     * Allow callers to get the list of monitors for use in sending data to them.
     */
    public List<Monitor> getMonitors()
    {
        return monitors;
    }

    /**
     * Enable registration processing.
     */
    public void enable()
    {
        enabled = true;
    }

    /**
     * Disable registration processing.
     */
    public void disable()
    {
        enabled = false;
    }

    /**
     * Provide UUID to any caller.
     */
    static public String getUUID()
    {
        return uuid;
    }
}
