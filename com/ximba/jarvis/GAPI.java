package com.ximba.jarvis;

/* Java */
import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.log4j.Logger;

// JSON-Simple
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * <p> Google API object that holds fields from a JSON return string. </p>
 * @author Michael J. Hammel
 * @since 1.0
 */
public class GAPI implements Serializable {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.jarvis.GAPI");

    // Default (no confidence) found setting
    static final double NOCONFIDENCE = -1.0;

    // Minimum confidence level.  
    // This should be configurable in the UI.
    static double MINCONFIDENCE = 0.8;

    // Default version number is 1.
    private int version = 1;

    // Status and ID from GAPI return
    private long status = -1;
    private String id = null;

    // Confidence level and text associated with it.
    private double confidence = NOCONFIDENCE;
    private String transcript  = null;

    /*
     * ---------------------------------------------------------------
     * Constructors
     * ---------------------------------------------------------------
     */

    /*
     * Constructor error checks the GAPI version.
     * Currently only versions 1 and 2 of the Google Speech API are supported.
     */
    public GAPI() throws Exception {
        int ver = Integer.parseInt( Cli.get(Cli.S_GAPIV) );
        switch( ver )
        {
            case 1: break;
            case 2: break;
            default: throw new Exception("Invalid Google Speech API version.");
        }
        this.version = ver;
    }

    /*
     * ---------------------------------------------------------------
     * Inner classes
     * ---------------------------------------------------------------
     */

    /*
     * ---------------------------------------------------------------
     * Private methods
     * ---------------------------------------------------------------
     */

    // Google Speech API V1: https://cloud.google.com/speech/reference/rest/v1/speech
    private void parseV1( String jsonStr )
    {
        try {
            log.info("V1 JSON: " + jsonStr);

            //parses input json string to JSONObject
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(jsonStr);

            // Retrieve the array of Results (what google thought was in the voice file).
            JSONArray data = null;
            try { data = (JSONArray)jsonObject.get("results"); }
            catch(Exception e) {}
            if ( data == null )
            {
                log.error ("Missing \"results\" field in JSON data.");
                return;
            }
             
            // Iterate over collection of possible responses. 
            Iterator it = data.iterator();
            while( it.hasNext() )
            {
                JSONObject result = (JSONObject)it.next();

                JSONArray alternative = null;
                try { alternative = (JSONArray)result.get("alternatives"); }
                catch(Exception e) {}
                if ( alternative == null )
                {
                    log.error ("Missing \"alternatives\" field in JSON data.");
                    return;
                }

                Iterator ait = alternative.iterator();
                while( ait.hasNext() )
                {
                    JSONObject entry = (JSONObject)ait.next();
                    String transcript = "n/a";
                    double confidence = (double)-1.0;

                    try { transcript = (String)entry.get("transcript"); }
                    catch(Exception e) { 
                        log.error ("Missing transcript.");  
                        continue; 
                    }
                    if ( transcript == null )
                    {
                        log.error ("Missing transcript.");  
                        continue; 
                    }
                
                    // Only concerned with those that have a confidence level.
                    try { confidence = (Double)entry.get("confidence"); }
                    catch(Exception e) {}
                    if ( confidence == -1.0 )
                        continue;

                    if ( confidence > this.confidence )
                    {
                        this.confidence = confidence;
                        this.transcript  = transcript;
    
                        log.info("Saved transcript: " + transcript);
                        log.info("Saved confidence: " + confidence);
                    }
                }
            }
        } catch (ParseException e) {
            log.info("Failed to parse JSON", e);
            e.printStackTrace();
        }
    }

    /*
     * ---------------------------------------------------------------
     * Public Methods
     * ---------------------------------------------------------------
     */

    /**
     * Parse a JSON string.  Results are kept internally.
     * @param jsonStr    The JSON string to parse, ie the return value from the Google Speech API.
     */
    public void parse( String jsonStr )
    {
        switch( version )
        {
            case 1: parseV1( jsonStr ); break;
        }
    }

    /**
     * Retrieve confidence level
     * @return The confidence level Google has in the transcription of the audio.
     */
    public double getConfidence()
    {
        return this.confidence;
    }

    /**
     * Retrieve transcript.
     * @return The audio converted to text, if any.
     */
    public String getTranscript()
    {
        return this.transcript;
    }

    /**
     * Get current confidence level.
     * @return The configured confidence level.
     */
    public double getMinConfidence() { return MINCONFIDENCE; }

    /**
     * Set current confidence level.
     * @param level The new confidence level.
     */
    public void setMinConfidence(double val) { MINCONFIDENCE = val; }

    /**
     * Get current no confidence level.
     * @return The configured no confidence level.
     */
    public double getNoConfidence() { return NOCONFIDENCE; }

}
