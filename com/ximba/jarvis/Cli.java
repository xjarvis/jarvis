package com.ximba.jarvis;

import java.io.*;
import java.util.*;
import java.net.*;
import org.apache.log4j.Logger;
import org.apache.commons.cli.*;

/* Project Imports */
import com.ximba.common.*;

/**
 * <p> 
 * Command line interface parser.
 * </p>
 * @author Michael J. Hammel
 * @since 1.0
 */

public class Cli {

    /** lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.jarvis.Cli");

    // A place to store configuration settings.
    private static final Map<String, String> cfg = new HashMap<String, String>();

    // Directory paths
    private static String runDir     = null;
    private static String runDataDir = null;
    private static String homeDir    = null;
    private static String jarvisDir  = null;
    private static String audioDir   = null;
    private static String memDir     = null;
    private static String apikeyFile = null;

    /* Help strings. */
    private static final String S_OPTION_VERBOSE  = "Enable verbose output";
    private static final String S_OPTION_H        = "Print this message";
    private static final String S_OPTION_VERSION  = "Display version";
    private static final String S_OPTION_GAPIV    = "Google API Version; supported versions: 1";

    // these are the named properties we use in our configuration mappings.
    private static final String S_VERBOSE         = "verbose";
    private static final String S_VERSION         = "version";
    private static final String S_ENABLED         = "enabled";

    public  static final String S_GAPIV           = "gapiv";
    public  static final String S_JARVISDIR       = "JARVISDIR";
    public  static final String S_AUDIODIR        = "AUDIODIR";
    public  static final String S_MEMDIR          = "MEMDIR";
    public  static final String S_RUNDIR          = "RUNDIR";
    public  static final String S_RUNDATADIR      = "RUNDATADIR";
    public  static final String S_APIKEY          = "APIKEY";

    // Prevent enable/disable except from main()
    private boolean inMain = false;

    /*
     * =======================================================
     * Private methods 
     * =======================================================
     */

    /*
     * Initialize some configuration items, some of which
     * may be overridden by command line options.
     */
    private void init() throws IOException
    {
        // Don't init more than once.
        if ( runDir != null )
            return;

        runDir    = System.getProperty("user.dir");
        runDataDir= runDir + File.separator + "data";
        homeDir   = System.getenv("HOME");
        jarvisDir = homeDir + File.separator + ".jarvis";
        audioDir  = jarvisDir + File.separator + "audio";
        memDir    = jarvisDir + File.separator + "memory";
        apikeyFile= jarvisDir + File.separator + "apikey";

        System.err.println("runDir    : " + runDir);
        System.err.println("runDataDir: " + runDataDir);
        System.err.println("homeDir   : " + homeDir);
        System.err.println("jarvisDir : " + jarvisDir);
        System.err.println("audioDir  : " + audioDir);
        System.err.println("memDir    : " + memDir);
        System.err.println("apikeyFile: " + apikeyFile);

        // Create initial directories
        File dir = new File ( audioDir );
        dir.mkdirs();
        dir = new File ( memDir );
        dir.mkdirs();

        // Initialize our configuration
        cfg.put(S_VERBOSE, "false");
        cfg.put(S_ENABLED, "false");
        cfg.put(S_GAPIV,   "1");

        // Make sure API Key exists.
        File file = new File( apikeyFile );
        if ( ! file.exists() )
        {
            throw new IOException("Missing API Key file");
        }
    }

    /*
     * =======================================================
     * Public methods 
     * =======================================================
     */

    /** 
      * Parse the command line.
      * @param args Command line arguments.
      */
    void parseCmdLine(String[] args) throws IOException
    {
        // Don't parse more than once.
        if ( runDir != null )
            return;

        inMain = true;
        init();

        /* Define our boolean options */
        Option o_help1      = new Option( "?", S_OPTION_H );
        Option o_help2      = new Option( "h", false, S_OPTION_H );
        Option o_verbose    = new Option( S_VERBOSE, false, S_OPTION_VERBOSE);
        Option o_version    = new Option( S_VERSION, false, S_OPTION_VERSION);

        /* Define options that take an argument */
        Option o_gapiv      = new Option( S_GAPIV, true, S_OPTION_GAPIV);

        Options options = new Options();
        options.addOption(o_help1);
        options.addOption(o_help2);
        options.addOption(o_verbose);
        options.addOption(o_version);
        options.addOption(o_gapiv);

        /* create the parser */
        CommandLineParser parser = new GnuParser();
        CommandLine line = null;

        /* parse the command line arguments */
        try {
            line = parser.parse( options, args );
        }
        catch( ParseException exp ) {
            String eMsg = "Parsing failed.  Reason: " + exp.getMessage();
            log.error( eMsg );
            System.err.println( eMsg );
            System.exit(1);
        }

        /* Print the version, if requested. */
        if ( line.hasOption( S_VERSION ) ) {
            System.err.println(BuildInfo.getVersion() );
            System.exit(0);
        }

        /* Set verbose output. */
        if ( line.hasOption( S_VERBOSE ) ) 
            cfg.put(S_VERBOSE, "true");

        /* Set Google API version. */
        if ( line.hasOption( S_GAPIV ) ) 
        {
            String verStr = line.getOptionValue( S_GAPIV );
            int verNum = Integer.parseInt(verStr);
            switch ( verNum )
            {
                case 1: break;
                default:
                    HelpFormatter formatter = new HelpFormatter();
                    formatter.printHelp( "jarvis", options );
                    System.err.println( "\nUnsupported version: " + verStr + "\n" +
                                        "Current configuration:\n" +
                                        "----------------------\n" +
                                        toString() );
                    System.exit(1);
                    break;
            }
            cfg.put(S_GAPIV, line.getOptionValue( S_GAPIV ));
        }

        /* Print the help message and exit, if requested. */
        if ( (line.hasOption( "h" )) || (line.hasOption( "?" )) ) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "jarvis", options );
            System.err.println( "\nCurrent configuration:\n" + 
                                "----------------------\n" + 
                                toString() );
            System.exit(0);
        }
    }

    /*
     * Enable the runtime.
     */
    void enable()
    {
        cfg.put(S_ENABLED, "true");
    }

    /*
     * Disable the runtime.
     */
    void disable()
    {
        cfg.put(S_ENABLED, "false");
    }

    /*
     * Check if we're configured for verbose mode.
     */
    static boolean isVerbose()
    {
        String val = cfg.get(S_VERBOSE);
        if ( (val==null) || val.equals("false") )
            return false;
        return true;
    }

    /*
     * Check if we're configured for verbose mode.
     */
    static boolean isEnabled()
    {
        String val = cfg.get(S_ENABLED);
        if ( (val==null) || val.equals("false") )
            return false;
        return true;
    }

    /*
     * Retrieve a configuration option.
     */
    static String get( String val )
    {
        if ( val == null )
            return null;

        // Immutable settings
        if ( val.equals(S_RUNDIR) )
            return runDir;
        if ( val.equals(S_RUNDATADIR) )
            return runDataDir;
        if ( val.equals(S_JARVISDIR) )
            return jarvisDir;
        if ( val.equals(S_AUDIODIR) )
            return audioDir;
        if ( val.equals(S_MEMDIR) )
            return memDir;
        if ( val.equals(S_APIKEY) )
            return apikeyFile;

        return cfg.get( val );
    }

    /*
     * Convert the name/value pairs to a printable string.
     */
    public String toString()
    {
        StringBuilder str = new StringBuilder("");

        // Immutables go first.
        str.append( "Jarvis directory: " + jarvisDir + "\n");
        str.append( "Audio directory: " + audioDir + "\n");

        // Then grab configurable items.
        for (Map.Entry<String, String> entry : cfg.entrySet())
            str.append( entry.getKey() + ": " + entry.getValue() + "\n");
        return str.toString();
    }
}
