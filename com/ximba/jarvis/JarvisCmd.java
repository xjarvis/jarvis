package com.ximba.jarvis;

/* Java */
import java.util.*;
import java.io.*;
import java.lang.*;
import java.net.*;
import org.apache.log4j.Logger;
import java.text.SimpleDateFormat;

// JSON-Simple
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/* Crypto */
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

// Jarvis
import com.ximba.common.*;

/**
 * <p> Commands Jarvis understands </p>
 * @author Michael J. Hammel
 * @since 1.0
 */
public class JarvisCmd {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.jarvis.JarvisCmd");

    /* Device list is a Message that gets updated frequently. */
    private static Message devices = null;

    // Register instance.
    private Register register = null;

    /*
     * ---------------------------------------------------------------
     * Constructors
     * ---------------------------------------------------------------
     */
    public JarvisCmd( Register register ){
        this.register = register;
    }

    /*
     * =======================================================
     * Public command analysis
     * =======================================================
     */

    /*
     * This is brute force command handling.  We look for keywords that match a command.
     * But we have to go through all possible commands till we find a match.  That's not
     * very efficient.
     */
    public void doCmd( NLP nlp )
    {
        if ( forJarvis(nlp) )
        {
            Message spk = new Message();
            if ( doQuit(nlp) )
            {
                spk.addData(Message.S_SPEAK, "Ok.  We'll talk later.");
                Speaker.submit(spk);

                Cli cli = new Cli();
                cli.disable();
            }
            else if ( doBasicCommands(nlp, spk) )
            {
                Speaker.submit(spk);
            }
            else if ( doStory(nlp) )
            {
                spk.addData(Message.S_SPEAK, "There once was a man from nantucket.");
                Speaker.submit(spk);
            }
            else if ( isAwake(nlp) )
            {
                log.info("Scheduling Awake response.");
                spk.addData(Message.S_SPEAK, "Yes, I'm here.  Can I help you?");
                Speaker.submit(spk);
            }
            else if ( enableRegister(nlp) )
            {
                log.info("Enabled registration.");
                spk.addData(Message.S_SPEAK, "Ok.  I've enabled monitor registration.");
                Speaker.submit(spk);
            }
            else if ( disableRegister(nlp) )
            {
                log.info("Disabled registration.");
                spk.addData(Message.S_SPEAK, "Ok.  I've disabled monitor registration.");
                Speaker.submit(spk);
            }
            else if ( iot(nlp,spk) )
            {
                log.info("IoT command delivered.");

                /* If there is a voice response, go ahead and schedule it. */
                if ( spk.getDataSize() > 0 )
                    Speaker.submit(spk);
            }
            else 
            {
                spk.addData(Message.S_SPEAK, "I'm sorry, I don't undstand: " + nlp.getCmd());
                Speaker.submit(spk);
            }
        }
    }

    /*
     * =======================================================
     * Private utility methods
     * =======================================================
     */

    /** 
     * Tell the time.
     */
    private void tellTime(Message msg)
    {
        StringBuilder today = new StringBuilder("Today is ");
        Date date=java.util.Calendar.getInstance().getTime();  
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMMM, d, YYYY,,");
        today.append(formatter.format(date));
        today.append(" at ");
        formatter = new SimpleDateFormat("h, m, aa,, zzzz");  
        today.append(formatter.format(date)); 
        msg.addData(Message.S_SPEAK, today.toString());
    }

    /** 
     * Tell the weather.
     */
    private void tellWeather(Message msg)
    {
        msg.addData(Message.S_SPEAK, "I don't know how to tell the weather yet.");
    }

    /*
     * =======================================================
     * Private command analysis
     * =======================================================
     */

    /** 
     * Check if this is a request for Jarvis
     * At least one spoken word must be "Jarvis" or we ignore it.
     */
    private boolean forJarvis(NLP nlp)
    {
        /* Look for recognized names first. */
        String[] words = nlp.getNames();
        if ( words == null )
            return false;
        for(int i=0; i<words.length; i++)
        {
            if ( words[i].equalsIgnoreCase("jarvis") )
                return true;
        }

        /* Name not recognized?  Try the nouns instead. */
        words = nlp.getNoun();
        if ( words == null )
            return false;
        for(int i=0; i<words.length; i++)
        {
            if ( words[i].equalsIgnoreCase("jarvis") )
                return true;
        }
        return false;
    }


    /** 
     * Check if user has asked us to exit.
     */
    private boolean doQuit(NLP nlp)
    {
        String[] token = nlp.getTokens();
        if ( token == null )
            return false;

        /* 
         * This must be a two word request so exit keywords can be used
         * in other contexts.
         */
        if ( token.length > 2 )
            return false;

        String words = "exit|quit";
        for(int i=0; i<token.length; i++)
        {
            if ( token[i].toLowerCase().matches("("+words+")") )
                return true;
        }
        return false;
    }

    /** 
     * Perform some basic commands, like tell me the time or temperature.
     */
    private boolean doBasicCommands(NLP nlp, Message spk)
    {
        int i, j;

        String[] token = nlp.getTokens();
        String[] tag = nlp.getTags();
        if ( tag != null )
        {
            for(i=1; i<tag.length; i++)
            {
                log.info( tag[i] + ":" + token[i] );
            }
        }

        log.info("doBasicCommands: tokens = " + Arrays.toString(token));
        if ( token.length <= 2 )
        {
            /* Maybe this wasn't an IoT request anyway. */
            log.info("iot: Not enough tokens");
            return false;
        }

        /* Is this a question? */
        boolean isQuestion = false;
        if ( (token != null) && (token.length > 0) && 
             (tag   != null) && (tag.length > 0) )
        {
            /* We can add more later for more complex requests */
            String words = "what";
            for(i=1; i<tag.length; i++)
            {
                if ( tag[i].startsWith("WP") && (token[i].toLowerCase().matches("("+words+")")) )
                {
                    isQuestion = true;
                    break;
                }
            }
        }

        boolean isCommand = false;
        if ( !isQuestion )
        {
            /* Is this a command? */
            token = nlp.getVerbs();
            log.info("doBasicCommands: verbs = " + Arrays.toString(token));
            if ( (token != null) && (token.length > 0) )
            {
                /* We can add more later for more complex requests */
                String words = "tell";
                for(i=1; i<token.length; i++)
                {
                    if ( token[i].toLowerCase().matches("("+words+")") )
                    {
                        isCommand = true;
                        break;
                    }
                }
            }
        }

        if ( !isQuestion && !isCommand )
        {
            /* Not for us. */
            return false;
        }

        /* This is for us.  Figure out what they want. */
        token = nlp.getNoun();
        log.info("doBasicCommands: nouns = " + Arrays.toString(token));
        String[] words = {"time", "weather"};
        if ( (token != null) && (token.length > 0) )
        {
            /* We can add more later for more complex requests */
            for(i=0; i<token.length; i++)
            {
                for(j=0; j<words.length; j++)
                {
                    if ( token[i].toLowerCase().equals(words[j]) )
                    {
                        switch(j)
                        {
                            case 0: tellTime(spk); break;
                            case 1: tellWeather(spk); break;
                        }
                        return true;
                    }
                }
            }
        }

        /* Nothing to do */
        return false;

    }

    /** 
     * Check if user asked for a story.
     */
    private boolean doStory(NLP nlp)
    {
        String[] token = nlp.getNoun();
        if ( token == null )
            return false;

        for(int i=0; i<token.length; i++)
        {
            if ( token[i].equalsIgnoreCase("story") )
                return true;
        }
        return false;
    }

    /** 
     * Check if Jarvis is awake
     * This looks for things like:
     *  are you awake
     *  are you there
     *  are you listening
     *  do you hear me
     *  can you hear me
     *  etc.
     */
    private boolean isAwake(NLP nlp)
    {
        int i=0;
        log.info("isAwake: cmd = " + nlp.getCmd());

        // Is this for Jarvis?
        String[] token = nlp.getTokens();
        log.info("isAwake: tokens = " + Arrays.toString(token));
        boolean toJarvis = false;
        String[] tag = nlp.getTags();
        log.info("isAwake: tags = " + Arrays.toString(tag));
        if ( tag != null )
        {
            String words = "are|do|can";
            for(i=1; i<tag.length; i++)
            {
                if ( tag[i].startsWith("PR") )
                {
                    if ( token[i].equalsIgnoreCase("you") )
                    {
                        if ( token[i-1].toLowerCase().matches("("+words+")") )
                        {
                            toJarvis = true;
                            break;
                        }
                    }
                }
            }
        }
        if ( !toJarvis )
            return false;
        log.info("isAwake: found " + token[i-1] + " " + token[i]);

        // Look for awake and similar
        token = nlp.getVerbs();
        if ( token != null )
        {
            String words = "listen|hear";
            for(i=0; i<token.length; i++)
            {
                if ( token[i].toLowerCase().matches("("+words+").*") )
                    return true;
            }
        }
        token = nlp.getAdverbs();
        if ( token != null )
        {
            String words = "awake|there";
            for(i=0; i<token.length; i++)
            {
                if ( token[i].toLowerCase().matches("("+words+").*") )
                    return true;
            }
        }
        return false;
    }

    /** 
     * Enable monitor registration.
     */
    private boolean enableRegister(NLP nlp)
    {
        // register.enable();
        return false;
    }

    /** 
     * Disable monitor registration.
     */
    private boolean disableRegister(NLP nlp)
    {
        // register.disable();
        return false;
    }

    /** 
     * Handle IoT requests.
     * Try to match the request to an IoT device. If a matching device is found, try to process the request.
     * We don't make a query of the device here.  We just use the cached state data from the Jarvis.DeviceQueryTask().
     * @param nlp   The NLP object holding the request.
     * @param msg   A message object to stuff replies into, as necessary.
     * @return True if the request looks like it belongs to an IoT device, false otherwise.
     */
    private boolean iot(NLP nlp, Message msg)
    {
        log.info("iot: Testing...");
        String[] tags = nlp.getTags();
        log.info("iot: tags = " + Arrays.toString(tags));

        String[] token = nlp.getTokens();
        log.info("iot: tokens = " + Arrays.toString(token));
        if ( token.length <= 2 )
        {
            /* Maybe this wasn't an IoT request anyway. */
            log.info("iot: Not enough tokens");
            return false;
        }

        /* Get available monitors, if any. */
        List<Monitor> monitors = register.getMonitors();
        if ( monitors.size() == 0 )
        {
            /* Maybe this wasn't an IoT request anyway. */
            log.info("iot: no monitors");
            log.error("No monitors registered.");
            return false;
        }
        log.info("Found " + monitors.size() + " monitors.");

        /* Check if any nouns or names match specific monitors. */
        List<Monitor> exactMonitor = new ArrayList<Monitor>();
        List<Monitor> closeMonitor = new ArrayList<Monitor>();
        String[] names = nlp.getNames();
        String[] nouns = nlp.getNoun();
        String[] nameAndNouns = Utils.concatStrings( names, nouns );

        log.info("iot: names = " + Arrays.toString(names));
        log.info("iot: nouns = " + Arrays.toString(nouns));
        if ( nameAndNouns != null )
        {
            for(int i=0; i<nameAndNouns.length; i++)
            {
                if ( nameAndNouns[i].equalsIgnoreCase("jarvis") )
                    continue;

                for ( Monitor monitor: monitors )
                {
                    String description = monitor.getDescription();
                    log.info("Description: search for \"" + nameAndNouns[i] + "\", in \"" + description + "\""); 
                    if ( nameAndNouns[i].equalsIgnoreCase(description) )
                    {
                        log.info("Exact match: " + nameAndNouns[i]);
                        exactMonitor.add(monitor);
                        continue;
                    }
                    else if ( nameAndNouns[i].matches("(.*)"+description+"(.*)") )
                    {
                        log.info("Close match.");
                        closeMonitor.add(monitor);
                    }
                }
            }
        }
        if ( (exactMonitor.size() > 1) || (closeMonitor.size() > 1) )
        {
            /* This was probably an IoT request but we can't identify who it's for. */
            log.info("iot: multiple matches");
            msg.addData( "multiple exact", "I found multiple matching monitors, could you be more specific?");
            return true;
        }

        Monitor monitor = null;
        if ( exactMonitor.size() == 1 )
        {
            monitor = exactMonitor.get(0);
        }
        else if ( closeMonitor.size() == 1 )
        {
            monitor = closeMonitor.get(0);
        }
        if (monitor == null)
        {
            /* Don't say anything here because this may not have been an IoT request. */
            log.info("iot: no matches");
            return false;
        }

        /*
         * From here on we assume this was an IoT request because we identified a specific monitor in the request.
         * Now we need to figure out which device they wanted from that monitor.
         * Then we need to figure out what they wanted with the specified device.
         */

        /* Get the device list as JSON data */
        JSONParser parser;
        JSONObject deviceListJson;
        JSONArray devices;
        try {
            parser=new JSONParser();
            deviceListJson =(JSONObject)parser.parse(monitor.getDeviceList());
            log.info("Device list for requested monitor: " + monitor.getDeviceList());
            devices = (JSONArray) deviceListJson.get("devices");
        }
        catch(Exception e){
            log.error("Failed to parse IoT request: " + nlp.getCmd());
            String response = new String( "I'm sorry. I can't see devices for that monitor." );
            log.info(response);
            msg.addData( Message.S_SPEAK + "01", response);
            return true;
        }

        /* Iterate devices for this monitor, looking for matching names/nouns. */
        List<String> exactMatch = new ArrayList<String>();
        List<String> closeMatch = new ArrayList<String>();
        String description = monitor.getDescription();
        try {
            Iterator it = devices.iterator();
            while (it.hasNext()) 
            {
                JSONObject device = (JSONObject)it.next();
                String uuid = (String)device.get("uuid");
                String type = (String)device.get("type");
                description = (String)device.get("description");

                /* Is device a match? */
                for(int i=0; i<nameAndNouns.length; i++)
                {
                    // Since we have multiple pseudonums, look for exact matches first then close matches.
                    String typeName;
                    List<String> types = Device.getPseudonyms(type);
                    Iterator typeIt = types.iterator();
                    while (typeIt.hasNext()) 
                    {
                        typeName = (String)typeIt.next();
                        if ( nameAndNouns[i].equalsIgnoreCase(typeName) )
                        {
                            log.info("Exact match: " + nameAndNouns[i]);
                            exactMatch.add(uuid+":"+description);
                            break;
                        }
                    }
                    if ( exactMatch.size() == 0 )
                    {
                        typeIt = types.iterator();
                        while (typeIt.hasNext()) 
                        {
                            typeName = (String)typeIt.next();
                            if ( nameAndNouns[i].matches("(.*)"+typeName+"(.*)") )
                            {
                                log.info("Close match.");
                                closeMatch.add(type);
                                break;
                            }
                        }
                        if ( nameAndNouns[i].equalsIgnoreCase(description) )
                        {
                            log.info("Exact match: " + nameAndNouns[i]);
                            closeMatch.add(type);
                            break;
                        }
                        else if ( nameAndNouns[i].matches("(.*)"+description+"(.*)") )
                        {
                            log.info("Close match.");
                            closeMatch.add(type);
                            break;
                        }
                    }
                }
            }
        }
        catch(Exception e){
            log.error("Failed to parse IoT request: " + nlp.getCmd());
            String response = new String("I'm sorry.  I'm having a problem getting device information.");
            log.info(response);
            msg.addData( Message.S_SPEAK + "01", response);
            return true;
        }

        /* If no exact matches then ask for clarification. */
        if ( exactMatch.size() == 0 )
        {
            Set<String> uniqueTypes = new HashSet<String>(closeMatch);

            String response = new String("I'm not sure which device you're asking for.");
            log.info(response);
            msg.addData( Message.S_SPEAK + "01", response);

            StringBuilder typeStrings = new StringBuilder("Is it one of these? ");
            int i = 0;
            Iterator it = uniqueTypes.iterator();
            {
                String typeName = (String)it.next();
                if ( i>0 )
                {
                    typeStrings.append( " or ");
                }
                typeStrings.append( typeName );
                i++;
            }
            log.info(typeStrings.toString());
            msg.addData( Message.S_SPEAK + "02", typeStrings.toString());
            return true;
        }

        /* We have an exact match.  Check for verbs, re: actions to perform on the device. */
        String[] verbs = nlp.getVerbs();
        String[] adverbs = nlp.getAdverbs();
        String[] verbsAndAdverbs = Utils.concatStrings( verbs, adverbs );
        log.info("iot: verbs = " + Arrays.toString(verbs));
        log.info("iot: adverbs = " + Arrays.toString(adverbs));
        String foundAction = null;
        String enableWords = "on|enable";
        String disableWords = "off|disable";
        if ( verbsAndAdverbs != null )
        {
            for(int i=0; i<verbsAndAdverbs.length; i++)
            {
                log.info("Checking " + verbsAndAdverbs[i] + " against on/enable.");
                if ( verbsAndAdverbs[i].toLowerCase().matches("("+enableWords+").*") )
                {
                    // Turn the device on
                    foundAction = new String("on");
                    break;
                }

                log.info("Checking " + verbsAndAdverbs[i] + " against off/disable.");
                if ( verbsAndAdverbs[i].toLowerCase().matches("("+disableWords+").*") )
                {
                    // Turn the device off
                    foundAction = new String("off");
                    break;
                }
            }
        }
        else
        {
            /*
             * No verbs or adverbs.  Maybe it's a just a phrase instead of a clause.
             * Try evaluating all words just ot make sure this isn't a state command.
             */
            for( int i=0; i<token.length; i++ )
            {
                /* Skip nouns */
                if ( tags[i].startsWith("NN") )
                    continue;

                log.info("Checking " + token[i] + " against on/enable.");
                if ( token[i].toLowerCase().matches("("+enableWords+").*") )
                {
                    // Turn the device on
                    foundAction = new String("on");
                    break;
                }
                log.info("Checking " + token[i] + " against off/disable.");
                if ( token[i].toLowerCase().matches("("+disableWords+").*") )
                {
                    // Turn the device off
                    foundAction = new String("off");
                    break;
                }
            }
        }

        /* If no verbs/adverbs, just state the status */
        if ( foundAction == null )
        {
            log.info("No action identified.  Will describe device states in general.");

            /* Load the device list into a JSON object */
            int numDevices = 0;
            int numOn = 0;
            int numOff = 0;
            try {
                Iterator it = devices.iterator();
                while (it.hasNext()) {
                    JSONObject device = (JSONObject)it.next();
                    String type = (String)device.get("type");
                    description = (String)device.get("description");
                    String state = (String)device.get("state");
                    log.info("Device: " + type + ", description: " + description + ", state = " + state);
                    numDevices++;
                    if ( state.equalsIgnoreCase("on") )
                        numOn++;
                    else
                        numOff++;
                }
            }
            catch(Exception e){
                log.error("Failed to parse IoT request: " + nlp.getCmd());
                return false;
            }

            String message = new String("I found " + numDevices + " devices.");
            log.info(message);
            msg.addData( Message.S_SPEAK + "01", message);

            message = new String( "" + numOn + " devices are enabled." );
            log.info(message);
            msg.addData( Message.S_SPEAK + "02", message);

            message = new String( "" + numOff + " devices are disabled." );
            log.info(message);
            msg.addData( Message.S_SPEAK + "03", message);

            return true;
        }

        /* If this was a command to enable/disable something... */
        if ( foundAction.toLowerCase().matches("(on|off)") ) 
        {
            /* If found, issue the action being requested. */
            Iterator it = exactMatch.iterator();
            while (it.hasNext()) 
            {
                String entry = (String)it.next();
                String[] fields = entry.split(":"); // [0]: uuid, [1]: description

                Imwww imwww = new Imwww();
                imwww.setDevice(monitor, fields[0], foundAction);
            }

            String state;
            if ( foundAction.toLowerCase().equals("on") )
                state="enabled";
            else
                state="disabled";

            StringBuilder message = new StringBuilder("Okay.  I have " + state);
            if ( exactMatch.size() > 1 )
                message.append( " them." );
            else
                message.append( " it." );
            log.info(message);
            msg.addData( Message.S_SPEAK + "01", message.toString());
            return true;
        }

        log.error("It's a command but we don't know what to do with it: " + nlp.getCmd());
        String message = new String("I'm sorry.  I don't know what to do with that.");
        log.info(message);
        msg.addData( Message.S_SPEAK + "01", message);
        return true;
    }

    /*
     * =======================================================
     * Public commands
     * =======================================================
     */

    /** 
     * Get Devices request.
     * @return A Message object that holds the set of monitors and their device lists. 
     * @note Each monitor can be retrieved by using the monitor IP as the name
     * to retrieve the associated Message data item, which then needs to be decoded
     * using the Message decode() method.
     */
    public static Message getDevices()
    {
        /* This command is for us.  Request devices list from the monitor. */
        Message response = null;
        Imwww imwww = new Imwww();
        try {
            /* 
             * Dummy string, so we can encrypt a message that can be decrypted on the other end for validation of
             * the sender. Otherwise anyone spoofing our IP could send the request.
             */
            String json = imwww.encode( new String("getDevices") );
            response = imwww.send("POST", "/devices", json);
        }
        catch(Exception e) {
            log.error("Failed /device request: " + e.getMessage());
            return null;
        }

        /*
         * There may not be any devices registered with the monitor yet.
         */
        if ( response.getDataSize() == 0 )
        {
            log.info("No devices registered yet.");
            return null;
        }

        /*
         * Return the Message, which holds a Map<> of monitor/devicelists.
         */
        return response;
    }

    public static Message getDevices(InetAddress addr)
    {
        /* This command is for us.  Request devices list from the monitor. */
        Message response = null;
        Imwww imwww = new Imwww();
        imwww.setAddress(addr);
        try {
            /* 
             * Dummy string, so we can encrypt a message that can be decrypted on the other end for validation of
             * the sender. Otherwise anyone spoofing our IP could send the request.
             */
            String json = imwww.encode( new String("getDevices") );
            response = imwww.send("POST", "/devices", json);
        }
        catch(Exception e) {
            log.error("Failed /device request: " + e.getMessage());
            return null;
        }

        /*
         * There may not be any devices registered with the monitor yet.
         */
        if ( response.getDataSize() == 0 )
        {
            log.info("No devices registered yet.");
            return null;
        }

        /*
         * Return the Message, which holds a Map<> of monitor/devicelists.
         */
        return response;
    }

    /** 
     * Put Devices request.
     */
    public static void putDevices(String json)
    {
        /* This command is for us.  Request devices list from the monitor. */
        Message response = null;
        Imwww imwww = new Imwww();
        try {
            response = imwww.send("PUT", "/device/" + Register.getUUID(), json);
        }
        catch(Exception e) {
            log.error("Failed PUT /device request: " + e.getMessage());
        }

        /* Response is JSON with AES encrypted message.  Get IV and message. */
        try {
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(response.getDataItem("response"));
            String iv      = (String) jsonObject.get("iv");
            String message = (String) jsonObject.get("message");

            if ( (iv==null) || (message==null) )
            {
                log.error("Incomplete response to /device: iv=" + iv + ", message=" + message);
                return;
            }

            /* Decode message and display it */
            AES aes = new AES();
            String status = aes.decrypt(message, Register.getUUID(), iv);
            log.info("Status: " + status );
        }
        catch(Exception e){
            log.error("Invalid response to PUT /device: " + e.getMessage());
        }

    }
}
