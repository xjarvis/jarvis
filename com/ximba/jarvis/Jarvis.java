package com.ximba.jarvis;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.file.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/* Project Imports */
import com.ximba.common.*;

/**
 * <p> 
 * Main class for Jarvis.
 * </p>
 * @author Michael J. Hammel
 * @since 1.0
 */

class Jarvis {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.jarvis.Jarvis");

    // Where we store log4j properties, relative to runtime directory.
    private static final String S_LOG4JPROPS = "config/log4j.properties";
    private static final String S_AUDIODIR   = ".jarvis/audio";

    // Run time Configuration 
    Cli cli = null;

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    public Jarvis() {}

    /*
     * =======================================================
     * Private classes
     * =======================================================
     */
    private class DeviceQueryTask extends TimerTask
    {
        private final Logger log = Logger.getLogger("com.ximba.jarvis.Jarvis.DeviceQueryTask");
        Register register = null;
        public DeviceQueryTask( Register obj )
        {
            this.register = obj;
        }

        public void run() 
        {
            List<Monitor> monitors = register.getMonitors();
            if ( monitors.size() == 0 )
            {
                log.info("No registered monitors yet.");
                return;
            }

            Message response = JarvisCmd.getDevices();
            if ( response != null )
            {
                for ( Monitor monitor: monitors )
                {
                    InetAddress addr = monitor.getAddress();
                    String deviceList = response.decode( addr.getHostAddress(), register );
                    if ( deviceList != null )
                    {
                        monitor.setDeviceList( deviceList );
                        log.info("Monitor: " + addr.getHostAddress());
                        log.info("Devices: \n" + deviceList);
                    }
                }
            }
        }
    }

    /*
     * =======================================================
     * Private methods
     * =======================================================
     */

    private void parseCmdLine( String[] args ) throws IOException
    {
        cli = new Cli();
        cli.parseCmdLine( args );
    }

    private void setup()
    {
        // Load log4j properties file.  It must be in a file relative to where we started.
        PropertyConfigurator.configure(S_LOG4JPROPS);

        System.err.println("Version: " + BuildInfo.getVersion());
        if ( Cli.isVerbose() ) 
        {
            System.err.println("Verbose: Enabled");
            System.err.println("Using Google API Version: " + cli.get(Cli.S_GAPIV));
        }

        // Deal with signals - not pretty but best case for Java.
        final Thread mainThread = Thread.currentThread();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                Cli cli = new Cli() ;
                cli.disable() ;
                try { mainThread.join(); }
                catch(Exception e) {}
            }
        });
    }

    private void go()
    {
        // start Recorder Thread
        Speech speech = new Speech();
        speech.setup();
        try { speech.start(); }
        catch(Exception e){}

        // start Speaker Thread
        Speaker speaker = new Speaker();
        speaker.setup(speech);
        try { speaker.start(); }
        catch(Exception e){}

        // start toText Thread
        ToText toText = new ToText();
        toText.setup();
        try { toText.start(); }
        catch(Exception e){}

        // Start Pair processing
        Register register = new Register();
        register.start();

        // start Cmd Thread
        Cmd cmd = new Cmd();
        cmd.setup();
        cmd.setRegister( register );
        try { cmd.start(); }
        catch(Exception e){}

        // start UI Thread
        UI ui = new UI();
        ui.setup();
        ui.setRegister( register );
        try { ui.start(); }
        catch(Exception e){}

        // Start Timer to retrieve devices list and state data.
        TimerTask task = new DeviceQueryTask(register);
        Timer timer = new Timer();
        timer.schedule(task, 1000,5000);

        System.out.println("You can start talking now.");

        // Enable processing and wait for exit.
        Utils.initTimeStamp();
        cli.enable();
        while( cli.isEnabled() )
        {
            try { Thread.sleep(0,100000); }
            catch(Exception e) {}
        }

        // Stop the Timer.
        timer.cancel();

        // Stop registration
        register.stop();

        // stop UI Thread
        ui.shutdown();

        // stop Cmd Thread
        cmd.shutdown();

        // stop toText Thread
        toText.shutdown();

        // stop Speech Thread
        speech.shutdown();

        // stop Speech Thread
        speaker.shutdown();

        log.info("Jarvis shutdown complete.");
    }

    /*
     * =======================================================
     * Main
     * Launch threads.
     * Then sit and spin till we get a shutdown notification.
     * =======================================================
     */
    public static void main(String[] args) {
        Jarvis jarvis = new Jarvis();
        try {
            jarvis.parseCmdLine( args );
        }
        catch (IOException ioe) {
            System.out.println(ioe.getMessage());
            System.exit(1);
        }
        jarvis.setup();
        jarvis.go();
    }
}
