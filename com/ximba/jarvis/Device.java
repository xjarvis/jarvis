package com.ximba.jarvis;

/* Java */
import java.util.*;
import java.net.*;
import java.io.*;
import java.lang.*;
import org.apache.log4j.Logger;

// JSON-Simple
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * <p> Class for managing device information. </p>
 * @author Michael J. Hammel
 * @since 1.0
 */
public class Device {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.jarvis.Device");

    /* Pseudonyms */
    private static final List<String> P_LIGHT = Arrays.asList(
            "light",
            "lights",
            "lamp",
            "dimmer",
            "light switch",
            "dimmer switch"
            );
    private static final List<String> P_BLIND = Arrays.asList(
            "blind",
            "blinds",
            "shade",
            "shades",
            "window blind",
            "window shade",
            "window blinds",
            "window shades"
            );

    /*
     * ---------------------------------------------------------------
     * Constructors
     * ---------------------------------------------------------------
     */

    public Device() {}

    /*
     * ---------------------------------------------------------------
     * Inner classes
     * ---------------------------------------------------------------
     */

    /*
     * ---------------------------------------------------------------
     * Private methods
     * ---------------------------------------------------------------
     */


    /*
     * ---------------------------------------------------------------
     * Public Methods
     * ---------------------------------------------------------------
     */

    /**
     * Returns a list of pseudonyms for a give device type.  This allows
     * us to associate different names for devices that can map to a specific
     * device type.
     * @param type  String type
     * @return A List<String> of pseudonums for the specified type or null if the type is unknown.
     */
    public static List<String> getPseudonyms ( String type )
    {
        switch(type){
            case "light_switch":
                return P_LIGHT;
            case "blind":
                return P_BLIND;
        }
        return(null);
    }
}
