package com.ximba.jarvis;

// Java
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.log4j.Logger;

// Jarvis
import com.ximba.common.*;

/**
 * <p> 
 * Command processing thread for Jarvis.
 * </p>
 * @author Michael J. Hammel
 * @since 1.0
 */

public class Cmd implements Runnable {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.jarvis.Cmd");

    /** Message queue - the list of files to process. */
    private static final ConcurrentLinkedQueue<Message> msgs = new ConcurrentLinkedQueue<Message>();
    private static final ConcurrentLinkedQueue<Message> commands = new ConcurrentLinkedQueue<Message>();

    // The register object, for enabling and disabling registration.
    private Register register = null;

    /*
     * Thread Managment
     */

    /* This thread. */
    private Thread thread = null;
    private Thread toActionThread = null;

    /* When true, the thread is running. */
    private boolean isRunning = false;
    private boolean toActionIsRunning = false;

    /* When true, the thread has completed and is ready to change state to "not running". */
    private boolean runComplete = false;

    /* When shutting down, disable exception hanlding in run() method. */
    private boolean doShutdown = false;

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    void Cmd() { }

    /*
     * =======================================================
     * Inner classes
     * =======================================================
     */

    /*
     * Convert commands to action
     */
    class ToAction implements Runnable
    {
        public void run(){
            toActionIsRunning = true;
            while ( toActionIsRunning )
            {
                Message msg = null;
                synchronized( commands )
                {
                    msg = commands.poll();
                }
                if ( msg != null )
                {
                    long start = Utils.getTimeStamp();

                    String cmd = msg.getDataItem(Message.S_CMD);
                    if ( cmd == null )
                        continue;

                    // Parse command into various NLP components.
                    NLP nlp = null;
                    try {
                        nlp = new NLP(cmd);
                        nlp.tokenize();
                        nlp.pos();
                        nlp.names();
                        log.info( nlp.toString() );
                    }
                    catch(Exception e){
                        log.info( "Error while performing NLP on command.", e);
                        continue;
                    }

                    // Tell the UI about timings for message processing.
                    Message timings = new Message();
                    timings.addData( Message.S_NLPMSG, "" );
                    timings.addData( Message.S_TS_SPIN, msg.getDataItem(Message.S_TS_SPIN) );
                    timings.addData( Message.S_TS_SPOUT, msg.getDataItem(Message.S_TS_SPOUT) );
                    timings.addData( Message.S_TS_TTIN, msg.getDataItem(Message.S_TS_TTIN) );
                    timings.addData( Message.S_TS_TTOUT, msg.getDataItem(Message.S_TS_TTOUT) );
                    timings.addData( Message.S_TS_JSIN, msg.getDataItem(Message.S_TS_JSIN) );
                    timings.addData( Message.S_TS_JSOUT, msg.getDataItem(Message.S_TS_JSOUT) );
                    timings.addData( Message.S_TS_NLPIN, ""+start );
                    timings.addData( Message.S_TS_NLPOUT, ""+Utils.getTimeStamp() );
                    UI.submit( timings );

                    JarvisCmd jarvisCmd = new JarvisCmd(register);
                    jarvisCmd.doCmd( nlp );
                }
                else
                {
                    try { Thread.sleep(0,100000); }
                    catch(Exception e){}
                }
            }
        }
    }

    /*
     * =======================================================
     * Private methods
     * =======================================================
     */

    /** 
     * Handle inbound Cmd messages.
     */
    private void process(Message msg)
    {
        if ( msg == null )
            return;

        long start = Utils.getTimeStamp();

        String json = msg.getDataItem(Message.S_JSON);
        if ( json == null )
        {
            log.error("JSON response missing from message. Ignoring.");
            return;
        }

        try {
            GAPI gapi = new GAPI();
            gapi.parse( json );
            Message pmsg = new Message();
            if ( (gapi.getTranscript() != null) && (gapi.getConfidence() >= gapi.getMinConfidence()) )
            {
                // We got something that is recognizable speech.
                log.info("Transcript okay: " + gapi.getTranscript());
                pmsg.addData( Message.S_CMD, gapi.getTranscript() );
                pmsg.addData( Message.S_TS_SPIN, msg.getDataItem(Message.S_TS_SPIN) );
                pmsg.addData( Message.S_TS_SPOUT, msg.getDataItem(Message.S_TS_SPOUT) );
                pmsg.addData( Message.S_TS_TTIN, msg.getDataItem(Message.S_TS_TTIN) );
                pmsg.addData( Message.S_TS_TTOUT, msg.getDataItem(Message.S_TS_TTOUT) );
                pmsg.addData( Message.S_TS_JSIN, ""+start );
                pmsg.addData( Message.S_TS_JSOUT, ""+Utils.getTimeStamp() );
                synchronized( commands )
                {
                    commands.add( pmsg );
                }
            }
            else if ( (gapi.getTranscript() != null) && (gapi.getConfidence() != gapi.getNoConfidence() ) )
            {
                // We got something that looks like speech but we couldn't quite make it out.
                log.info( String.format("Min/No Confidence: %f, %f", gapi.getMinConfidence(), gapi.getNoConfidence()) );
                log.info( String.format("NoConfidence (%f) Transcript: %s", gapi.getConfidence(), gapi.getTranscript()) );
                pmsg.addData( Message.S_SPEAK, "I'm sorry, what did you say?");
                Speaker.submit( pmsg );
            }
            else 
            {
                // This ignores things like key clicks when you're too close to the microphone.
                log.info("Ignoring noise." );
            }
        }
        catch(Exception e) {
            log.error("Failed to parse JSON.");
        }
    }

    /*
     * =======================================================
     * Public Methods - Thread Managmenet
     * =======================================================
     */

    /** 
     * Submit a message to this thread.
     * The queue is thread-safe.
     */
    public static void submit(Message msg) 
    {
        synchronized (msgs)
        {
            if ( msg != null )
                msgs.add( msg );
        }
    }

    /** 
     * Setup Cmd specific items.
     */
    boolean setup() 
    {
        if ( !isRunning )
        {
            return true;
        }
        return false;
    }

    /** 
     * Store the registration object.
     */
    void setRegister( Register register )
    {
        this.register = register;
    }

    /** 
     * Start the thread.
     * @return True if the thread is started, false if it is already running.
     * @throws IOException if the thread cannot start.
     */
    synchronized boolean start() throws IOException
    {
        /*
         * Create and start the thread.
         */
        if ( !isRunning )
        {
            // Clear the inbound queue when we start, since its a class variable.
            synchronized (msgs)
            {
                msgs.clear();
            }

            this.thread = new Thread(this, "Cmd");
            thread.start();

            // ToAction thread start
            toActionThread = new Thread( new ToAction(), "SPToAct");
            toActionThread.start();

            /* Wait for thread to start. */
            Calendar startWait = Calendar.getInstance();
            while ( !isRunning )
            {
                Calendar now = Calendar.getInstance();
                now.add(Calendar.SECOND, (-1*5));
                if ( now.after(startWait) )
                    break;
            }
            if ( !isRunning )
            {
                thread.interrupt();
                thread = null;
                runComplete = false;
                throw new IOException("Timed out waiting for server thread to start.");
            }
            return true;
        }
        else
            return false;
    }

    /** 
     * Stop the thread.  This always succeeds and can be used to make
     * sure the thread is ready to be started again.
     */
    synchronized void shutdown()
    {
        doShutdown = true;
        if ( isRunning )
        {
            toActionIsRunning = false;
            isRunning = false;
            thread.interrupt();

            /* Wait for the thread to stop. */
            Calendar startWait = Calendar.getInstance();
            while ( !runComplete )
            {
                Calendar now = Calendar.getInstance();
                now.add(Calendar.SECOND, (-1*5));
                if ( now.after(startWait) )
                    break;
            }
            log.info("Cmd shutdown complete.");
        }
        doShutdown = false;
    }

    /** 
     * Handle inbound Cmd messages.
     */
    public void run()
    {
        if ( isRunning )
            return;
        runComplete = false;

        /*
         *----------------------------------------------------------------
         * Spin, waiting on shutdown.
         *----------------------------------------------------------------
         */
        log.info("Starting Cmd Thread.");
        isRunning = true;
        while ( isRunning )
        {
            Message msg = null;
            synchronized( msgs )
            {
                msg = msgs.poll();
            }
            if ( msg != null )
                process(msg);
            else
            {
                try { Thread.sleep(0,100000); }
                catch(Exception e){}
            }
        }
        runComplete = true;
        log.info("Cmd thread exiting.");
    }
}
