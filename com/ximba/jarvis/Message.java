package com.ximba.jarvis;

/* Java */
import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.log4j.Logger;

// JSON-Simple
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

// Jarvis
import com.ximba.common.*;

/**
 * <p> Generic message wrapper for Jarvis </p>
 * @author Michael J. Hammel
 * @since 1.0
 */
public class Message implements Serializable {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.jarvis.Message");

    // Main threads message forwarding.
    public static final String S_FILE       = "FILE";
    public static final String S_CMD        = "CMD";
    public static final String S_JSON       = "JSON";
    public static final String S_GAPI       = "GAPI";
    public static final String S_SPEAK      = "SPEAK";

    // Component timestamps - how to follow a message through the system.
    public static final String S_TS_SPIN    = "TSSPIN";     // When audio is recordered
    public static final String S_TS_SPOUT   = "TSSPOUT";    // When audio is queued for conversion
    public static final String S_TS_TTIN    = "TSTTIN";     // When audio conversion starts
    public static final String S_TS_TTOUT   = "TSTTOUT";    // When audio conversion is complete
    public static final String S_TS_JSIN    = "TSJSIN";     // When JSON processing starts
    public static final String S_TS_JSOUT   = "TSJSOUT";    // When JSON processing completes
    public static final String S_TS_NLPIN   = "TSNLPIN";    // When natural language processessing (NLP) starts
    public static final String S_TS_NLPOUT  = "TSNLPOUT";   // When natural language processing completes

    // Audio Processing Messages
    public static final String S_AUDIOMSG   = "AUDIOMSG";
    public static final String S_RAW        = "RAW";
    public static final String S_RECCNT     = "RECCNT";
    public static final String S_WHISPERS   = "WHISPERS";
    public static final String S_AUDIOLEVEL = "AUDIOLEVEL";

    // NLP Processing Messages
    public static final String S_NLPMSG     = "NLPMSG";
    public static final String S_TBD        = "TBD";

    // API Processing Messages
    public static final String S_APIMSG     = "APIMSG";
    public static final String S_XMIT       = "XMIT";
    public static final String S_REPLIES    = "REPLIES";
    public static final String S_EMPTY      = "EMPTY";
    public static final String S_CONF       = "CONF";
    public static final String S_NOCONF     = "NOCONF";
    public static final String S_NOISE      = "NOISE";

    // IoT Processing Messages
    public static final String S_IOTMSG     = "IOTMSG";
    public static final String S_DEVCNT     = "DEVCNT";
    public static final String S_DEVID      = "DEVID";

    // Where we store data for each message.
    private Map<String, String>  data       = new HashMap<String, String>();
    private Map<String, byte[]>  byteData   = new HashMap<String, byte[]>();

    /*
     * ---------------------------------------------------------------
     * Constructors
     * ---------------------------------------------------------------
     */
    public Message() {}

    /**
     * Copy constructor.
     * @param msg   The source object to copy.
     * @throws NullPointerException if msg argument is null.
     */
    public Message( Message msg ) {

        if ( msg == null )
            throw new NullPointerException();

        // data
        for (Map.Entry<String, String> entry : msg.getData().entrySet())
        {
            String name = (String)entry.getKey();
            String value = new String( (String)entry.getValue() );
            data.put( name, value ); 
        }

        // byte data
        for (Map.Entry<String, byte[]> bentry : msg.getByteData().entrySet())
        {
            String name = new String((String)bentry.getKey());
            byte[] value = (byte[])bentry.getValue();
            byte[] destValue = new byte[ value.length ];
            for (int i=0; i<value.length; i++)
                destValue[i] = value[i];
            byteData.put( name, destValue ); 
        }
    }

    /*
     * ---------------------------------------------------------------
     * Setters
     * ---------------------------------------------------------------
     */

    /** 
     * Add a string data item 
     * @param name   A symbollic name for the item that is to be added
     * @param value  The associated value for the item that is to be added
     * @throws NullPointerException if the name or value arguments are null.
     */
    public void addData(String name, String value) throws NullPointerException { 
        if ( name == null ) 
            throw new NullPointerException("name is null");
        if ( value == null )
            throw new NullPointerException("value is null");
        data.put( name, value ); 
    }

    /** 
     * Add a byte data item 
     * @param name   A symbollic name for the item that is to be added
     * @param value  The associated byte array for the item that is to be added
     * @throws NullPointerException if the name or value arguments are null.
     */
    public void addData(String name, byte[] value) throws NullPointerException { 
        if ( name == null ) 
            throw new NullPointerException("name is null");
        if ( value == null )
            throw new NullPointerException("value is null");
        byteData.put( name, value ); 
    }

    /** 
     * Delete a string data item 
     * @param name   The symbollic name of the item that is to be deleted
     * @throws NullPointerException if the name argument is null.
     */
    public void removeData(String name) throws NullPointerException { 
        if ( name == null ) 
            throw new NullPointerException();
        data.remove( name ); 
    }

    /** 
     * Delete a byte data item 
     * @param name   The symbollic name of the item that is to be deleted
     * @throws NullPointerException if the name argument is null.
     */
    public void removeByteData(String name) throws NullPointerException { 
        if ( name == null ) 
            throw new NullPointerException();
        byteData.remove( name ); 
    }

    /*
     * ---------------------------------------------------------------
     * Getters
     * ---------------------------------------------------------------
     */

    /** 
     * Get all string data itmes
     * @return A Map collection of String/String pairs.
     */
    public Map<String, String> getData() { return data; }

    /** 
     * Get number of items in the String data.
     * @return An integer value >= 0.
     */
    public int getDataSize() { return data.size(); }

    /** 
     * Get the value for a named string data item
     * @param name The name of the string data item to search for
     * @return The String value associated with the provided name or NULL if no match is available.
     */
    public String getDataItem(String name) { return data.get(name); }

    /** 
     * Get all byte data itmes
     * @return A Map collection of String/byte[] pairs.
     */
    public Map<String, byte[]> getByteData() { return byteData; }

    /** 
     * Get number of items in the Byte data.
     * @return An integer value >= 0.
     */
    public int getByteDataSize() { return byteData.size(); }

    /** 
     * Get the value for a named byte data item
     * @param name The name of the data item to search for
     * @return The byte[] value associated with the provided name or NULL if no match is available.
     */
    public byte[] getByteDataItem(String name) { return byteData.get(name); }

    /**
     * Retrieve the string key/value pairs in the item collection.
     */
    public String toString() 
    { 
        StringBuilder sb = new StringBuilder("");
        for (Map.Entry<String, String> entry : data.entrySet())
        {
            String key = entry.getKey();
            String value = entry.getValue();
            sb.append( key + ":" + value + "\n" );
        }
        return sb.toString();
    }

    /**
     * Decode a message.
     * @param name The name of the string data item to decode
     * @param register The registration object holding the set of available monitors.
     * @return The decoded string or NULL if no match is available.
     * @note This assumes the requested entry is a JSON object with Base64 encoded iv and message fields
     * where the message is also encrypted with the key from this Jarvis instance.
     */
    public String decode(String name, Register register)
    { 
        /* Find the entry. */
        String item = data.get(name);

        /* Decode message and store it */
        try {
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(item);
            String iv      = (String) jsonObject.get("iv");
            String message = (String) jsonObject.get("message");

            if ( (iv==null) || (message==null) )
            {
                log.error("Incomplete encoded message for " + name);
                return null;
            }

            /* Decode message and return it */
            AES aes = new AES();
            String decrypted_message = aes.decrypt(message, register.getUUID(), iv);
            return decrypted_message;
        }
        catch(Exception e){
            log.error("Failed to parse message for " + name);
            return null;
        }
    }
}
