package com.ximba.jarvis;

/* Java */
import java.util.*;
import java.net.*;
import java.io.*;
import java.lang.*;
import org.apache.log4j.Logger;

// JSON-Simple
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * <p> Class for managing monitor connections. </p>
 * @author Michael J. Hammel
 * @since 1.0
 */
public class Monitor {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.jarvis.Monitor");

    /* Instance variables */
    InetAddress     address = null;    // Address of remote monitor
    String          descriptor = null; // Descriptor of the monitor
    String          deviceList = null; // JSON data for device list for this monitor

    /*
     * ---------------------------------------------------------------
     * Constructors
     * ---------------------------------------------------------------
     */

    public Monitor() {}

    /*
     * ---------------------------------------------------------------
     * Inner classes
     * ---------------------------------------------------------------
     */

    /*
     * ---------------------------------------------------------------
     * Private methods
     * ---------------------------------------------------------------
     */


    /*
     * ---------------------------------------------------------------
     * Public Methods
     * ---------------------------------------------------------------
     */

    /**
     * Sets the registration information for the monitor.
     */
    public void register( InetAddress address, String descriptor )
    {
        this.address = address;
        this.descriptor = descriptor;
    }

    /**
     * Set descriptor of this monitor.
     * @param description   The description reported by the monitor.
     */
    public void setDescription( String description )
    {
        this.descriptor = description;
    }

    /**
     * Get IP address of this monitor.
     * @return IPAddress
     */
    public InetAddress getAddress()
    {
        return address;
    }

    /**
     * Get descriptor of this monitor.
     * This is a free-format string that can be searched for keywords.
     * @return String
     */
    public String getDescription()
    {
        return descriptor;
    }

    /**
     * Test descriptor for the specified string.
     * @return boolean
     */
    public boolean isLike( String needle )
    {
        return descriptor.contains(needle);
    }

    /**
     * Test if the supplied IP address against this monitor's IP address.
     * @return boolean
     */
    public boolean matches( InetAddress candidate )
    {
        log.info("Comparing monitor addr " + address.getHostAddress() + " with canidate addr " + 
                candidate.getHostAddress());
        if ( address.getHostAddress().equals( candidate.getHostAddress() ) )
        {
            log.info("They match.");
            return true;
        }
        log.info("They don't match.");
        return false;
    }

    /**
     * Set device list for this monitor.
     * @param json   The JSON data holding the device list information for this monitor.
     */
    public void setDeviceList( String json )
    {
        this.deviceList = json;
    }

    /**
     * Get device list for this monitor.
     * @return The JSON data (as a String) holding the device list information for this monitor.
     */
    public String getDeviceList()
    {
        return(this.deviceList);
    }
}
