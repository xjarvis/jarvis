package com.ximba.jarvis;

// Java
import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.log4j.Logger;

import org.json.simple.JSONObject;

// Jarvis
import com.ximba.common.*;

/**
 * <p> 
 * Convert a speech file to text via Google's Speech-to-Text API.
 * This utilizes Google's Cloud Speech API.
 * </p>
 * @author Michael J. Hammel
 * @since 1.0
 * @see https://cloud.google.com/speech/
 */

public class ToText implements Runnable {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.jarvis.ToText");

    /* Message queue - the list of files to process. */
    private static final ConcurrentLinkedQueue<Message> msgs = new ConcurrentLinkedQueue<Message>();

    /*
     * Thread Managment
     */

    /* This thread. */
    private Thread thread = null;

    /* When true, the thread is running. */
    private boolean isRunning = false;

    /* When true, the thread has completed and is ready to change state to "not running". */
    private boolean runComplete = false;

    /* When shutting down, disable exception hanlding in run() method. */
    private boolean doShutdown = false;

    /* Read the API key at startup */
    private static String apikey = null;

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    void ToText() 
    { 
    }

    /*
     * =======================================================
     * Inner classes
     * =======================================================
     */

    /*
     * =======================================================
     * Private methods
     * =======================================================
     */

    /** 
     * Handle inbound ToText messages.
     */
    @SuppressWarnings("unchecked")
    public void process(Message msg)
    {
        if ( msg == null )
            return;

        long start = Utils.getTimeStamp();

        // Pull filename from message
        String audioFile = msg.getDataItem(Message.S_FILE);
        if ( audioFile == null )
        {
            log.error("audioFile in message is null. Ignoring.");
            return;
        }

        try {
            // File must be relative to audio directory.
            File file = new File( Cli.get(Cli.S_AUDIODIR) + File.separator + audioFile );
            if ( !file.exists() )
            {
                log.error("No such file: " + audioFile);
                return;
            }
    
            log.info("Reading file: " + file.getPath());
            Path path = Paths.get( file.getPath() );
            byte[] data = Base64.getEncoder().encode( Files.readAllBytes(path) );
            file.delete();
            if ( (data == null) || (data.length==0) )
            {
                log.error("No data in audio file.  Skipping.");
                return;
            }
            
            /* 
             * Build a request string to Google's Speech API
             * Users must register to get their own API key and store it in ~/.jarvis/apikey
             */
            String request = "https://speech.googleapis.com/v" + Cli.get(Cli.S_GAPIV) + "/speech:recognize?key=" + apikey;

            /* Create the JSON body with the request to convert audio to text.
             * Format of JSON body
            {
                "config": {
                    "encoding":"FLAC",
                    "sample_rate": 16384,
                    "language_code": "en-US"
                },
                "audio": {
                    "content":"base64 encoded audio data"
                }
            }
            */

            JSONObject config = new JSONObject();
            JSONObject audio  = new JSONObject();
            JSONObject parent = new JSONObject();

            config.put("encoding","FLAC");
            config.put("sampleRateHertz", new Integer(16000));
            config.put("languageCode", "en-US");

            audio.put("content", new String(data));

            parent.put("config", config);
            parent.put("audio", audio);
    
            // Setup the connection to Google
            URL url = new URL(request); 
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();           
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(false); 
            connection.setRequestMethod("POST"); 
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8"); 
            connection.setRequestProperty("User-Agent", "speech2text"); 
            connection.setConnectTimeout(60000);
            connection.setUseCaches (false);
            
            // Submit the request.
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
            wr.write( parent.toString().getBytes("UTF-8") );
            wr.flush();
            wr.close();
            connection.disconnect();
           
            // Pull the results
            BufferedReader in = new BufferedReader( new InputStreamReader(connection.getInputStream()) );
            String decodedString;
            StringBuilder response = new StringBuilder("");
            while ((decodedString = in.readLine()) != null) {
                response.append(decodedString + "\n");
            }
            log.info("Response: " + response.toString());
            if ( response.toString().length() == 0 )
            {
                log.error("No data in response from Google.  Skipping.");
                return;
            }
    
            // Submit the request the Command Processor.
            Message jmsg = new Message();
            jmsg.addData( Message.S_TS_SPIN, msg.getDataItem(Message.S_TS_SPIN) );
            jmsg.addData( Message.S_TS_SPOUT, msg.getDataItem(Message.S_TS_SPOUT) );
            jmsg.addData( Message.S_TS_TTIN, ""+start );
            jmsg.addData( Message.S_TS_TTOUT, ""+Utils.getTimeStamp() );
            jmsg.addData(Message.S_JSON, response.toString());
            Cmd.submit( jmsg );
        }
        catch(Exception e){
            log.error("Exception while submitting audio to Google.", e);
        }
    }

    /*
     * =======================================================
     * Public Methods - Thread Managmenet
     * =======================================================
     */

    /** 
     * Submit a message to this thread.
     * The queue is thread-safe.
     */
    public static void submit(Message msg) 
    {
        synchronized (msgs)
        {
            if ( msg != null )
                msgs.add( msg );
        }
    }

    /** 
     * Setup ToText specific items.
     */
    boolean setup() 
    {
        if ( !isRunning )
        {
            return true;
        }
        return false;
    }

    /** 
     * Start the thread.
     * @return True if the thread is started, false if it is already running.
     * @throws IOException if the thread cannot start.
     */
    synchronized boolean start() throws IOException
    {
        /*
         * Create and start the thread.
         */
        if ( !isRunning )
        {
            /* First time, read in the API key */
            if ( apikey == null ) 
            {
                try {
                    apikey = Utils.readFile( Cli.get(Cli.S_APIKEY), Charset.defaultCharset() );
                }
                catch (IOException ioe) {
                    apikey="NoAPIKeyAvailable";
                }
                log.info("API key: " + apikey);
            }

            // Clear the inbound queue when we start, since its a class variable.
            synchronized (msgs)
            {
                msgs.clear();
            }

            this.thread = new Thread(this, "ToText");
            thread.start();

            /* Wait for thread to start. */
            Calendar startWait = Calendar.getInstance();
            while ( !isRunning )
            {
                Calendar now = Calendar.getInstance();
                now.add(Calendar.SECOND, (-1*5));
                if ( now.after(startWait) )
                    break;
            }
            if ( !isRunning )
            {
                thread.interrupt();
                thread = null;
                runComplete = false;
                throw new IOException("Timed out waiting for server thread to start.");
            }
            return true;
        }
        else
            return false;
    }

    /** 
     * Stop the thread.  This always succeeds and can be used to make
     * sure the thread is ready to be started again.
     */
    synchronized void shutdown()
    {
        doShutdown = true;
        if ( isRunning )
        {
            isRunning = false;
            thread.interrupt();

            /* Wait for the thread to stop. */
            Calendar startWait = Calendar.getInstance();
            while ( !runComplete )
            {
                Calendar now = Calendar.getInstance();
                now.add(Calendar.SECOND, (-1*5));
                if ( now.after(startWait) )
                    break;
            }
            log.info("ToText shutdown complete.");
        }
        doShutdown = false;
    }

    /** 
     * Handle inbound ToText messages.
     */
    public void run()
    {
        if ( isRunning )
            return;
        runComplete = false;

        /*
         *----------------------------------------------------------------
         * Spin, waiting on shutdown.
         *----------------------------------------------------------------
         */
        log.info("Starting ToText Thread.");
        isRunning = true;
        while ( isRunning )
        {
            Message msg = null;
            synchronized( msgs )
            {
                msg = msgs.poll();
            }
            if ( msg != null )
                process(msg);
            else
            {
                try { Thread.sleep(0,100000); }
                catch(Exception e){}
            }
        }
        runComplete = true;
        log.info("ToText thread exiting.");
    }
}
