package com.ximba.jarvis;

// Java
import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import javax.sound.sampled.*;
import org.apache.log4j.Logger;

// Flac Encoder
import javaFlacEncoder.FLACEncoder;
import javaFlacEncoder.FLACFileOutputStream;
import javaFlacEncoder.StreamConfiguration;

// Jarvis
import com.ximba.common.*;

/**
 * <p> 
 * Speech processing thread for Jarvis.
 * </p>
 * @author Michael J. Hammel
 * @since 1.0
 */

public class Speech implements Runnable {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.jarvis.Speech");

    /*
     * Recorded snippets go in a queue for processing into files.
     */
    private ConcurrentLinkedQueue<Snippet> snippets = new ConcurrentLinkedQueue<Snippet>();

    /*
     * Thread Managment
     */

    /* Threads. */
    private Thread thread = null;
    private Thread writeAudioThread = null;

    /* When true, the thread is running. */
    private boolean isRunning = false;
    private boolean writeIsRunning = false;

    /* When true, the thread has completed and is ready to change state to "not running". */
    private boolean runComplete = false;

    /* When shutting down, disable exception hanlding in run() method. */
    private boolean doShutdown = false;

    /*
     * Audio configuration
     */

    private static boolean disabled     = false;
    private static Boolean disabledSync = new Boolean(false);

    final static float MAX_8_BITS_SIGNED    = Byte.MAX_VALUE;
    final static float MAX_8_BITS_UNSIGNED  = 0xff;
    final static float MAX_16_BITS_SIGNED   = Short.MAX_VALUE;
    final static float MAX_16_BITS_UNSIGNED = 0xffff;

    /* Audio Format defines the parameters of an audio recording. */
    AudioFormat audioFormat = null;

    /* TargetDataLine is the input device. */
    TargetDataLine line = null;

    /* Minimum level above which we believe speech is heard, between 0 and 1. */
    // This needs to be adjustable via the UI.
    private static float minLevel = 0.15f;

    boolean use16Bit  = false;
    boolean signed    = false;
    boolean bigEndian = false;

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    void Speech() { }

    /*
     * =======================================================
     * Inner classes
     * =======================================================
     */

    /*
     * Class used to store byte data from audio recording, before writing to a file.
     */
    class Snippet 
    {
        private byte[] bytes = null;
        public long timeStamp = Utils.getTimeStamp();

        public void setBytes( byte[] data )
        {
            bytes = data;
        }

        public byte[] getBytes()
        {
            return bytes;
        }

        public int size()
        {
            if ( bytes != null )
                return bytes.length;
            else
                return 0;
        }
    }

    /*
     * Class that handles writing an audio file to disk.
     * All files written here in in WAV format.
     * Conversion to FLAC based on:
     * https://github.com/The-Shadow/java-speech-api/blob/master/src/com/darkprograms/speech/recognizer/FlacEncoder.java
     */
    class WriteAudio implements Runnable
    {
        public void run(){
            writeIsRunning = true;
            while ( writeIsRunning )
            {
                // Grab a snippet, if any are available.
                Snippet snippet = snippets.poll();
                if ( snippet == null )
                    continue;
                if ( snippet.size() == 0 )
                {
                    log.error("No bytes in snippet!");
                    continue;
                }
                log.info("Writing snippet of " + snippet.size() + " bytes");

                // Get HOME/.jarvis/audio path, plus RandomGUID.toString()
                String filename = new RandomGUID().toString();
                File audioFile = new File( Cli.get(Cli.S_AUDIODIR) + File.separator + filename );

                // Prep for FLAC conversion from WAV
                StreamConfiguration streamConfiguration = new StreamConfiguration();
                streamConfiguration.setSampleRate( (int)audioFormat.getSampleRate() );
                streamConfiguration.setBitsPerSample( audioFormat.getSampleSizeInBits() );
                streamConfiguration.setChannelCount( audioFormat.getChannels() );

                try{
                    ByteArrayInputStream bis = new ByteArrayInputStream( snippet.getBytes() );
                    AudioInputStream ais = new AudioInputStream(bis, audioFormat, (int)(snippet.size() / audioFormat.getFrameSize()) );
                    int frameSize = audioFormat.getFrameSize();

                    // Write WAV file
                    /*
                    AudioSystem.write(
                        ais,
                        AudioFileFormat.Type.WAVE,
                        audioFile);
                    */

                    // Write to FLAC: See 
                    FLACFileOutputStream fos = new FLACFileOutputStream(audioFile);
                    FLACEncoder flacEncoder = new FLACEncoder();
                    flacEncoder.setStreamConfiguration(streamConfiguration);
                    flacEncoder.setOutputStream(fos);
                    flacEncoder.openFLACStream();

                    // Have bytes, now we need ints for use in flac encoding.
                    int frameLength = (int) ais.getFrameLength();
                    if(frameLength <= AudioSystem.NOT_SPECIFIED)
                        frameLength = 16384;  //Arbitrary file size
                    int[] sampleData = new int[frameLength];
                    byte[] samplesIn = new byte[frameSize];
                    int i = 0;
                    while (ais.read(samplesIn, 0, frameSize) != -1) 
                    {
                        if (frameSize != 1) 
                        {
                            ByteBuffer bb = ByteBuffer.wrap(samplesIn);
                            bb.order(ByteOrder.LITTLE_ENDIAN);
                            short shortVal = bb.getShort();
                            sampleData[i] = shortVal;
                        } 
                        else 
                        {
                            sampleData[i] = samplesIn[0];
                        }
                        i++;
                    }

                    // No need to convert to little endian - we already did that.
                    flacEncoder.addSamples( sampleData, i);
                    flacEncoder.encodeSamples(i, false);
                    flacEncoder.encodeSamples(flacEncoder.samplesAvailableToEncode(), true);

                    ais.close();
                    fos.close();

                    // Submit the saved audio file to ToText for conversion to text.
                    Message msg = new Message();
                    msg.addData( Message.S_FILE, filename );
                    msg.addData( Message.S_TS_SPIN, ""+snippet.timeStamp );
                    msg.addData( Message.S_TS_SPOUT, ""+Utils.getTimeStamp() );
                    ToText.submit( msg );

                    // Submit to UI for wave form display
                    ByteBuffer byteBuffer = ByteBuffer.allocate(sampleData.length*4);
                    IntBuffer intBuffer = byteBuffer.asIntBuffer();
                    intBuffer.put( sampleData );
                    msg = new Message();
                    msg.addData(Message.S_AUDIOMSG, "");
                    msg.addData(Message.S_RAW, byteBuffer.array());
                    UI.submit(msg);

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    /*
     * =======================================================
     * Private Methods
     * =======================================================
     */

    /*
     * These should be set via a UI.
     * For now, they are hard coded.
     */
    private AudioFormat getAudioFormat()
    {
        // Possible sampling rates: 8000,11025,16000,22050,44100
        float sampleRate = 16000.0F;

        // Number of bits per sample: 8,16
        int sampleSizeInBits = 16;

        // Number of channels: 1,2
        int channels = 1;

        //true,false
        boolean signed = true;

        //true,false
        boolean bigEndian = false;

        return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
    }

    /*
     * Calculate the line level volume for an audio clip.
     * See http://stackoverflow.com/questions/5800649/detect-silence-when-recording
     */
    private float calculateLevel (byte[] buffer, int readPoint, int leftOver) 
    {
        float level = (float)0;
        int max     = 0;

        if (use16Bit) 
        {
            for (int i=readPoint; i<buffer.length-leftOver; i+=2) 
            {
                int value = 0;

                // deal with endianness
                int hiByte = (bigEndian ? buffer[i] : buffer[i+1]);
                int loByte = (bigEndian ? buffer[i+1] : buffer [i]);

                if (signed) 
                {
                    short shortVal = (short) hiByte;
                    shortVal = (short) ((shortVal << 8) | (byte) loByte);
                    value = shortVal;
                } 
                else 
                {
                    value = (hiByte << 8) | loByte;
                }
                max = Math.max(max, value);
            }
        } 
        else 
        {
            // 8 bit - no endianness issues, just sign
            for (int i=readPoint; i<buffer.length-leftOver; i++) 
            {
                int value = 0;
                if (signed) 
                {
                    value = buffer [i];
                } 
                else 
                {
                    short shortVal = 0;
                    shortVal = (short) (shortVal | buffer [i]);
                    value = shortVal;
                }
                max = Math.max (max, value);
            }
        }

        // Convert max level (an int) to a float ranging from 0.0 to 1.0.
        // That's the percentage of the maximum volume.
        // The louder the audio, the more likely it's out speaker.
        if (signed) 
        {
            if (use16Bit) { level = (float) max / MAX_16_BITS_SIGNED; }
            else          { level = (float) max / MAX_8_BITS_SIGNED;  }
        } 
        else 
        {
            if (use16Bit) { level = (float) max / MAX_16_BITS_UNSIGNED; }
            else          { level = (float) max / MAX_8_BITS_UNSIGNED;  }
        }

        return level;
    }

    /*
     * =======================================================
     * Public Methods - Thread Managmenet
     * =======================================================
     */

    /** 
     * Setup for audio capture.
     */
    boolean setup() 
    {
        if ( isRunning )
            return true;

        try{
            audioFormat = getAudioFormat();
            DataLine.Info dataLineInfo = new DataLine.Info( TargetDataLine.class, audioFormat);
            line = (TargetDataLine) AudioSystem.getLine(dataLineInfo);

            // Setup for calculating level
            use16Bit  = (audioFormat.getSampleSizeInBits() == 16);
            signed    = (audioFormat.getEncoding() == AudioFormat.Encoding.PCM_SIGNED);
            bigEndian = audioFormat.isBigEndian();

            log.info("Setup of audio input is complete.");
            return true;
        }
        catch (LineUnavailableException lue) 
        {
            log.error("No available TargetDataLine found.", lue);
            return false;
        }
        catch (IllegalArgumentException iae) 
        {
            log.error("No TargetDataLine found that supports specified format.", iae);
            return false;
        }
        catch (Exception e) 
        {
            log.error("Failed to setup audio input.", e);
            return false;
        }
    }

    /** 
     * Start the thread.
     * @return True if the thread is started, false if it is already running.
     * @throws IOException if the thread cannot start.
     */
    synchronized boolean start() throws IOException
    {
        /*
         * Create and start the thread.
         */
        if ( !isRunning )
        {
            // Open the audio device and start it.
            try { 
                line.open(audioFormat);
                line.start();
            }
            catch(Exception e) {
                log.error("Failed to open audio line.", e);
                return false;
            }

            // Main thread start
            this.thread = new Thread(this, "Speech");
            thread.start();

            // Write audio thread start
            writeAudioThread = new Thread( new WriteAudio(), "SPWrite");
            writeAudioThread.start();

            /* Wait for main thread to start. */
            Calendar startWait = Calendar.getInstance();
            while ( !isRunning )
            {
                Calendar now = Calendar.getInstance();
                now.add(Calendar.SECOND, (-1*5));
                if ( now.after(startWait) )
                    break;
            }
            if ( !isRunning )
            {
                thread.interrupt();
                thread = null;
                runComplete = false;
                throw new IOException("Timed out waiting for server thread to start.");
            }
            return true;
        }
        else
            return false;
    }

    /** 
     * Stop the thread.  This always succeeds and can be used to make
     * sure the thread is ready to be started again.
     */
    synchronized void shutdown()
    {
        doShutdown = true;
        if ( isRunning )
        {
            writeIsRunning = false;
            isRunning = false;
            thread.interrupt();
            writeAudioThread.interrupt();

            /* Wait for the thread to stop. */
            Calendar startWait = Calendar.getInstance();
            while ( !runComplete )
            {
                Calendar now = Calendar.getInstance();
                now.add(Calendar.SECOND, (-1*5));
                if ( now.after(startWait) )
                    break;
            }
            log.info("Speech shutdown complete.");
        }
        doShutdown = false;
    }

    /** 
     * Handle inbound speech snippets.
     */
    public void run()
    {
        if ( isRunning )
            return;
        runComplete = false;

        // Setup input buffers.
        int bufferSize = (int) audioFormat.getSampleRate() * audioFormat.getFrameSize();
        byte buffer[] = new byte[bufferSize];

        // Create a place to save audio that is considered "speech".
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        /*
         *----------------------------------------------------------------
         * Spin, waiting on shutdown.
         *----------------------------------------------------------------
         */
        log.info("Starting Speech Listener.");
        isRunning = true;
        int reccnt = 0;
        int whispers = 0;
        int loopCnt = 0;
        boolean doRecord = false;
        Message msg = null;
        while ( isRunning )
        {
            /* We disable recording while Jarvis is speaking */
            synchronized(disabledSync) {
                if ( disabled )
                {
                    try { Thread.sleep(0,100000); }
                    catch(Exception e){}
                    continue;
                }
            }

            // Read a buffer of audio
            int count = line.read(buffer, 0, buffer.length);
            if ( count == 0 )
                continue;
            reccnt++;

            // Check if it is above background noise
            float level = calculateLevel(buffer,0,0);
            log.info("Audio level for inbound buffer: " + level);
            msg = new Message();
            msg.addData(Message.S_AUDIOMSG, "");
            msg.addData(Message.S_AUDIOLEVEL, ""+level);
            UI.submit(msg);

            // If not recording but level is high enough, then start recording
            if ( !doRecord )
            {
                if ( (level >=minLevel) )
                {
                    log.info("Started recording due to noise");
                    doRecord = true;
                }
                else
                    whispers++;
            }
            else
            {
                // If recording but level falls short for a given period, 
                // then save the snippet and prep for another.
                if ( (level < minLevel) )
                {
                    // Stop recording and save snippet
                    log.info("Stopped recording due to silence");
                    doRecord = false;
                    Snippet snippet = new Snippet();
                    snippet.setBytes( out.toByteArray() );
                    if ( snippet.size() != 0 )
                        snippets.add( snippet );

                    out.reset();
                }
            }

            if ( doRecord )
                out.write(buffer, 0, count);

            // Send status updates to the UI
            if (++loopCnt == 5)
            {
                msg = new Message();
                msg.addData(Message.S_AUDIOMSG, "");
                msg.addData(Message.S_RECCNT, ""+reccnt);
                msg.addData(Message.S_WHISPERS, ""+whispers);
                UI.submit(msg);
                reccnt = 0;
                whispers = 0;
                loopCnt = 0;
            }
        }
        line.stop();
        runComplete = true;
        log.info("Speech thread exiting.");
    }

    /**
     * Get current minimum audio level.
     * @return The configured minimum audio level.
     */
    public float getMinAudioLevel() { return minLevel; }

    /**
     * Set minimum audio level.
     * @param level The new audio level.
     */
    public void setMinAudioLevel(float val) { minLevel = val; }
    
    /**
     * Disable recording.  Usually called by the Speaker class.
     */
    synchronized public void disable() { 
        synchronized(disabledSync) {
            disabled = true; 
        }
    }

    /**
     * Enable recording.  Usually called by the Speaker class.
     */
    synchronized public void enable() { 
        synchronized(disabledSync) {
            disabled = false; 
        }
    }
}
